document.addEventListener("DOMContentLoaded", () => {
	let form = document.getElementById('form_a_archivo');
	form.addEventListener("submit", function(event){
		event.preventDefault();
		subir_archivos(this);
	});
});
 
  function subir_archivos(form){
    let barra_estado = form.children[1].children[0],
      span = barra_estado.children[0],
      btn_cancelar = form.children[2].children[1];
      console.log(form);
    barra_estado.classList.remove('barra_verde', 'barra_roja');
    let peticion = new XMLHttpRequest();
    peticion.upload.addEventListener("progress", (event) =>{
      let porcentaje = Math.round((event.loaded / event.total) * 100);
      console.log(porcentaje);
      barra_estado.style.width = porcentaje+'%';
      span.innerHTML = porcentaje+'%';
    });
    peticion.addEventListener("load", () => {
      barra_estado.classList.add('barra_verde');
      span.innerHTML = "PROCESS COMPLET";
    });
    // peticion.open('post', 'a_archivo.php');
    // peticion.send(new formData(form))
    btn_cancelar.addEventListener("click", () => {
      peticion.abort();
      barra_estado.classList.remove('barra_verde');
      barra_estado.classList.add('barra_roja');
      span.innerHTML = "PROCESS CANCEL";
    });
 }
