<?php
session_start();
if(isset($_SESSION['id_usuario'])){
  if ($_SESSION['status'] == '0') {
    header("location: admin.php");
  }else if ($_SESSION['status'] >= '1') {
    header('location: admin.php');
  }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>UTSEM</title>
  </head>
  <body>
  <div class="col-12 d-flex align-items-center justify-content-center">
    <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0"><br><br>
      <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
        <div class="card-header border-0" style="background-color: white;">
          <div class="card-title text-center">
            <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo">
          </div>         
          <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
            <span>ADMINISTRADOR</span>
          </h6>
          <div id="loginInfo"></div>
        </div>
        <div class="card-content" id="userFormContainer">
          <div class="card-body" style="padding-top: 0;">
            <span aria-hidden="true" id="mens"></span><hr>
            <form class="form-horizontal" method="POST" action="" id="formulg">
              <fieldset class="form-group position-relative">
                <input name="correo_usu" type="email" class="form-control" id="correo_usu"  placeholder="USUARIO" required pattern="[a-zA-Z0-9.-_#|\!%+-:;]+@[a-zA-Z0-9.-_#|\!%+-:;]+\.[a-zA-Z.|\!]{4,}$" title="Ingresa el usuario con el que te registraste.">
                <div class="form-control-position">
                </div>
              </fieldset>
              <fieldset class="form-group position-relative has-icon-left">
                <div class="input-group mb-3">
                   <input name="password_usu" type="password" class="form-control" id="password_usu" placeholder="**********************************************************************" required data-container="body" data-toggle="popover" data-trigger="focus" data-placement="left" data-content="Ingresa tu contraseña correctamente.">
                <div class="input-group-append">
                  <span class="btn btn-outline-primary btn-block" onclick="mostrarContrasena()"><i id="icon" class="fa fa-eye fa-1x"></i></span>
                </div>
              </div>

                <div class="form-control-position"></div>
              </fieldset>
             <input type="submit" name="login" id="login" class="btn btn-outline-primary btn-block"><!-- </button> -->
             <!--  <a class="btn btn-outline-primary btn-block" href="admin.php" role="button">Entrar</a>-->
               <a class="btn btn-outline-primary btn-block" href="index.php" role="button">Regresar</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

   <<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script type="text/javascript">
$(document).on('submit', '#formulg', function(event){
  event.preventDefault();

  $.ajax({
    url: 'v_l.php',
    type: 'POST',
    dataType: 'json',
    data: $(this).serialize(),
    beforeSend: function(){
      $('#login').val('validando usuario...');
    }
  })
  .done(function(respuesta){
    if (!respuesta.error) {
      if (respuesta.tipo == '0') {
        location.href = 'admin.php'
      $('#mens').fadeIn().html('<div class="alert alert-success text-center" role="alert">Iniciando session...</div>');
        setTimeout(function(){ $('#mens').fadeOut(); }, 2500);
      }else if (respuesta.tipo >= '1') {
        location.href = 'admin.php';
      $('#mens').fadeIn().html('<div class="alert alert-success text-center" role="alert">Iniciando session...</div>');
        setTimeout(function(){ $('#mens').fadeOut(); }, 2500);
      }
    }else{
      $('#mens').html('<div class="alert alert-danger" role="alert">Ocurrio un error valide sus credenciales</div>');
      setTimeout(function(){
        $('#mens').html('<div class="alert alert-warning" role="alert">escriba sus datos correctamente</div>');
      },3000);
      $('#login').val('Reiniciar');
    }
  })
  .fail(function(resp){
  })
  .done(function(){
    console.log("error");
  });
});

function mostrarContrasena(){
      var transform = document.getElementById("password_usu");
      if(transform.type == "password"){
          transform.type = "text";
          $('#icon').removeClass('fa fa-eye fa-1x').addClass('fa fa-eye-slash fa-1x');
      }else{
          transform.type = "password";
          $('#icon').removeClass('fa fa-eye-slash fa-1x').addClass('fa fa-eye fa-1x');
      }
}
    </script>
  </body>
</html>