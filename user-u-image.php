<?php
session_start();
require("conexion.php");
if ($_SESSION['solicitante'] == '') {
  header('Location: l-usuario.php');
}

$matricula =$_SESSION['solicitante']['matricula'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>UTSEM | Subir foto</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
 	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
 	
 	<style>
 	    body {
            margin:0;
            padding:0;
            font-family:sans-serif;
            background:#fbfbfb;
        }
        .card {
            background:#fff;
            box-shadow:0 20px 50px rgba(0,0,0,.1);
            border-radius:10px;
        }
        .card:hover {
            box-shadow:0 30px 70px rgba(0,0,0,.2);
        }
        .card .box .img {
            width:120px;
            margin:0 auto;
            border-radius:50%;
            overflow:hidden;
        }
        .card .box .img img {
            width:100%;
            height:100%;
        }
        .card .box h2 {
            font-size:20px;
            color:#262626;
            margin:20px auto;
        }
        .card .box h2 span {
            font-size:14px;
            background:#e91e63;
            color:#fff;
            display:inline-block;
            padding:4px 10px;
            border-radius:15px;
        }
        .card .box p {
            color:#262626;
        }
        .card .box span {
            display:inline-flex;
        }
 	</style>
 	
</head>
<body>
    
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img src="img/logo-utsem.png" width="50" height="50" class="d-inline-block align-top" alt="">
        </a>
        <a type="submit" class="btn btn-outline-primary btn-md" href="desconectar.php" role="button">Salir</a>
    </nav>
    <br>
    
    <?php require 'user-form_cam_foto.php'; ?>
    
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-12">
                    <h2 style="background: #00a48d; padding: 5px; color: #fff; border-radius: 5px;">Primer paso (Obligatorio)</h2>
                    <h6>Necesitamos una foto de perfil tuya</h6>
                    <div class="modal-body" style="color: gray;">
                        Caracteristicas:<br>
                        1.- Fondo blanco.<br>
                        2.- El cabello largo por atr&aacute;s de los hombros.<br>
                        3.- Rostro descubierto (sin fleco o barba).<br>
                        4.- Sin gorra, lente, aretes, collares u otros accesorios.<br><br>
                        Ejemplo: <img src="./img/foto_ejemplo.jpg" width="250">
                    </div><br>
                    <div class="alert alert-success" id="alerta1" style="display: none;">
                        Foto de perfil actualizada y en proceso de verificación
                    </div>
                    <div class="alert alert-warning" id="alerta2" style="display: none;">
                        No se pudo subir la foto
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="box text-center p-5">
                            <div class="img">
                                <img id="img" src="https://www.worksible.com//user_images/empty-image.png">
                            </div>
                            
                          
                            <h2 style="color:gray;"><?php echo utf8_encode($_SESSION['solicitante']['Nombre']);?></h2>
                            <!--<button class="btn btn-info btn-sm" id="btn-subir"><i class="fa fa-upload"></i> Subir una foto</button>-->
                            <button class="btn btn-info btn-sm" id="btn-tomar" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-camera-retro"></i> Tomar una foto</button>
                            <div id="subir-img" class="text-right" style="display: none;">
                                <br>
                                <form method="POST" id="form-subir" action="">
                                    <input type="file" name="foto" id="foto-subir" style="font-size: 12px;" accept="image/*"><br><br>
                                    <input type="button" id="btn-cancelar-subir" class="btn btn-default btn-sm" value="Cancelar" >
                                    <input type="submit" id="btn-submit-subir" class="btn btn-info btn-sm" value="Subir foto" >
                                </form>
                            </div>
                            <br><br>
                            <p>
                                <small style="color: gray;">
                                    Tu foto de perfil es obligatoria, sin este paso no podrás subir tus otros documentos.
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tomar una foto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary">Subir foto</button>
                </div>
            </div>
        </div>
    </div>-->
    
<script>
    $(document).ready(function(){
        $("#btn-subir").click(function(){
            $("#subir-img").fadeIn();
        });
        $("#btn-cancelar-subir").click(function(){
            $("#form-subir").trigger("reset");
            $("#subir-img").fadeOut();
        });
        $("#form-subir").submit(function(e){
            e.preventDefault();
            if($("#foto-subir").get(0).files.length === 0){
                alert("Debes seleccionar una imágen");
            }
            return false;
        });
        $("#btn-tomar").click(function(){
            $("#subir-img").fadeOut();
            $("#fotos_tomar").modal("show");
            return false;
        });
        
        $("#form_foto_webcam").on("submit", function() {
          event.preventDefault();
            $.ajax({
              url: 'a_fotos.php',        
              type: $(this).attr('method'),
              dataType: 'json',
              data: $(this).serialize(),
              cache: false,
              beforeSend: function(){
                $("#fotos_tomar").modal("hide");
                $("#btn-tomar").text("Subiendo foto");
                $("#btn-tomar").attr("disabled", "disabled");
                /*$("#btn-subir").attr("disabled", "disabled");*/
                /*$("#img").attr("src", "https://www.sic.gov.co/sites/all/modules/contrib/buscadorsic/css/images/loading.gif");*/
              },
              success: function(respuesta) {
              if(respuesta.error.error == false) {
                $('#alerta1').show();
                $("#img").attr("src", respuesta.error.nombre);
                $("#img").css({
                    'width': '120px',
                    'height': '120px'
                });
                setTimeout(function(){
                    location.href='user-form.php';
                }, 3000);
              } else if(respuesta.error.error == true){
                  $("#btn-tomar").text("Tomar foto");
                $("#btn-tomar").removeAttr("disabled");
                $('#alerta2').show;
              }
              },
              error: function(respuesta){
                $("#alerta2").show();
              }
            });
            return false;
          });
    })
</script>
	
</body>
</html>