-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 11-07-2020 a las 18:33:18
-- Versión del servidor: 5.6.47
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `utsemmor_sonline`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE `archivos` (
  `id_archivo` int(11) NOT NULL,
  `id_documento` int(11) NOT NULL,
  `id_solicitante` int(11) NOT NULL,
  `solicitante` varchar(85) NOT NULL,
  `carrera` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(85) CHARACTER SET utf8mb4 NOT NULL,
  `prorroga` varchar(2) NOT NULL,
  `archivo` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `ruta_archivo` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `tipo_archivo` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `comentario` varchar(60) CHARACTER SET utf8mb4 NOT NULL,
  `status` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id_documento` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `obligatorio` varchar(2) NOT NULL,
  `descripcion` varchar(80) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id_documento`, `id_usuario`, `nombre`, `obligatorio`, `descripcion`, `date_create`, `date_update`, `status`) VALUES
(4, 3, 'Acta de nacimiento', 'no', '', '2020-06-04 15:04:32', '2020-06-09 22:22:26', 1),
(6, 3, 'Curp', 'no', '', '2020-06-04 15:07:26', '2020-06-09 22:23:10', 1),
(10, 3, 'Recibo', 'si', '', '2020-06-09 22:20:07', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotos`
--

CREATE TABLE `fotos` (
  `id_foto` int(11) NOT NULL,
  `nombre` varchar(300) CHARACTER SET utf8mb4 NOT NULL,
  `id_solicitante` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fotos`
--

INSERT INTO `fotos` (`id_foto`, `nombre`, `id_solicitante`, `date_create`, `status`) VALUES
(10, 'fotos/foto_202010    ARZATE MUÃ‘OZ DIEGO ENRIQUEGASTRONOMIA5f0a413a7dc47.png', 202010, '2020-07-11 17:46:34', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos`
--

CREATE TABLE `periodos` (
  `id_periodo` int(11) NOT NULL,
  `periodo` varchar(45) NOT NULL,
  `periodo_inicio` date NOT NULL,
  `periodo_fin` date NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodos`
--

INSERT INTO `periodos` (`id_periodo`, `periodo`, `periodo_inicio`, `periodo_fin`, `date_create`, `date_update`, `status`) VALUES
(75, 'SEP-DIC 20', '2020-06-01', '2020-08-10', '2020-06-03 13:39:56', '2020-06-04 15:20:33', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prorroga`
--

CREATE TABLE `prorroga` (
  `id_prorroga` int(11) NOT NULL,
  `id_solicitante` int(11) NOT NULL,
  `fecha_entrega` date NOT NULL,
  `status` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitantes`
--

CREATE TABLE `solicitantes` (
  `id_solicitante` int(11) NOT NULL,
  `solicitante` varchar(85) COLLATE utf8_unicode_ci NOT NULL,
  `carrera` varchar(65) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(85) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `solicitantes`
--

INSERT INTO `solicitantes` (`id_solicitante`, `solicitante`, `carrera`, `email`, `status`, `date_create`, `date_update`) VALUES
(0, '', '', '', 0, '2020-07-09 21:36:12', '2020-07-09 22:31:10'),
(202010, 'ARZATE MUÑOZ DIEGO ENRIQUE', 'GASTRONOMIA', 'Diegoenerique620@gmail.com', 0, '2020-07-08 23:12:56', '2020-07-11 17:46:34'),
(2020000015, 'CUEVAS MELGAR ESTRELLA', 'TI ÁREA ENTORNOS VIRTUALES Y NEGOCIOS DIGITALES', 'estrellamelgarcicm@gmail.com', 0, '2020-07-09 13:05:28', '2020-07-11 17:19:41'),
(2020000016, 'CRUZ OCAMPO EDUARDO', 'GASTRONOMIA', 'Laloazul@gmail.com', 0, '2020-07-09 23:18:31', '2020-07-10 00:07:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usu` varchar(45) NOT NULL,
  `correo_usu` varchar(85) NOT NULL,
  `password_usu` varchar(50) NOT NULL,
  `inf_email` varchar(50) NOT NULL,
  `date_create` varchar(45) NOT NULL,
  `date_update` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usu`, `correo_usu`, `password_usu`, `inf_email`, `date_create`, `date_update`, `status`) VALUES
(3, 'Javier Casimiro Morales', 'javiercasimiro@utsem-morelos.edu.mx', 'NDlTY2QyZDZSVWxVendPOHI3OVZXdz09', '', '20-05-01 17:26:55', '2020-06-02', 1),
(7, 'Maricruz', 'maricruzaguilar@utsem-morelos.edu.mx', 'd05odVFXV0dIamdVQmlLdldkWnFQM3Q3VytiYms4RnlpRllGVl', '', '20-06-04 15:09:08', '2020-06-28', 3),
(8, 'Alma Flores Avila', 'almaflores@utsem-morelos.edu.mx', 'YmJOZnJvd2tNb2xyWU1ZZTJEV1ZZZz09', '', '20-06-09 19:44:47', '0000-00-00', 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD PRIMARY KEY (`id_archivo`),
  ADD KEY `id_documento` (`id_documento`),
  ADD KEY `id_solicitante` (`id_solicitante`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id_documento`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`id_foto`);

--
-- Indices de la tabla `periodos`
--
ALTER TABLE `periodos`
  ADD PRIMARY KEY (`id_periodo`);

--
-- Indices de la tabla `prorroga`
--
ALTER TABLE `prorroga`
  ADD PRIMARY KEY (`id_prorroga`),
  ADD KEY `id_solicitante` (`id_solicitante`);

--
-- Indices de la tabla `solicitantes`
--
ALTER TABLE `solicitantes`
  ADD PRIMARY KEY (`id_solicitante`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivos`
--
ALTER TABLE `archivos`
  MODIFY `id_archivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id_documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `fotos`
--
ALTER TABLE `fotos`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `periodos`
--
ALTER TABLE `periodos`
  MODIFY `id_periodo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT de la tabla `prorroga`
--
ALTER TABLE `prorroga`
  MODIFY `id_prorroga` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `archivos`
--
ALTER TABLE `archivos`
  ADD CONSTRAINT `archivos_ibfk_1` FOREIGN KEY (`id_documento`) REFERENCES `documentos` (`id_documento`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD CONSTRAINT `documentos_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
