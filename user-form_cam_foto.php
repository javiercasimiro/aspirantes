<style type="text/css">
  @media only screen and (max-width: 1800px) {
      video {
        max-width: 100%;
      }
    }
</style>

<div class="modal fade" id="fotos_tomar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content" style="background-color: #f3f3f3;">
	      <div class="modal-header">
	      	<div>
			  <select name="listaDeDispositivos" id="listaDeDispositivos" class="form-control"></select>	
             	<div class="row">
                   	<div class="col-sm">
                  		<img id="srcfoto" src="" style="position: sticky;margin-top:1rem;background-color: #fff;border: 1px solid #dee2e6;border-radius: .25rem;max-width: 100%;height: auto;">
                   	</div>
                	<div class="col-sm">
                  		<div style="color: #00a48d;border-color: #b8daff; margin:1rem; text-transform: uppercase;" id="estado"></div>
                	</div>
              	</div>
			</div>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">     	

			<video muted="muted" id="video" style="width: 100%; height:auto; border-radius: 5%; border: solid 5px #adb5bda8;"></video>
			<canvas id="canvas" style="display: none;"></canvas>

	      </div>
	      <div class="modal-footer">
			     <form id="form_foto_webcam" method="POST" action="">
	      	  <input type="hidden" name="nombre" id="nombre" value="">
	      		<input type="hidden" name="id_solicitante" value="<?php echo $_SESSION['solicitante']['matricula']?>">
            <button type="submit" class="btn btn-outline-primary" id="subirFoto" style="display: none;">SUBIR</button>
          </form>
	      	<button class="btn btn-outline-dark" id="boton" type="submit">TOMAR</button>
	      </div>
	    </div>
	  </div>
	</div>


  <script src="sc_fotos.js"></script>
  <script type="text/javascript">
    
  </script>