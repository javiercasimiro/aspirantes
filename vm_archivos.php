<?php
session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
    require("conexion.php");
    require("e_d.php");
    if(isset($_GET['id_solicitante'])) $id_solicitante = SED::descryption($_GET['id_solicitante']);
?>
<!DOCTYPE html>
<html lang="en">
  <head><meta charset="gb18030">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- <meta http-equiv="refresh" content="10"> -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <title>UTSEM</title>

    <style type="text/css">
   @media (min-width: 576px) {
    .image {
      width: 150px;
      height: 150px;
      /* background-color: #000; */
      border-radius: 50%;
      /* border-color: #fff; */
      overflow: hidden;
      position: absolute;
      /* border-width: 10px 5px 10px 5px; */
      /* border-style: solid; */
      border: solid 4px #f0f0f0;
      border-top-left-radius: 50% 50%;
      border-top-right-radius: 50% 50%;
      border-bottom-right-radius: 50% 50%;
      border-bottom-left-radius: 50% 50%;
      margin-left: 9.1%;
    }
    .image .fotimg{
       position: absolute;
       top: 50%;
       left: 50%;
       transform: translateX(-50%) translateY(-50%);
       max-width: 150%;
       max-height: 100%;
    }
    .butn {
      margin: 127px 0px 0px 77px;
    }
    .butnDos{
      width: 4.5%;
    }
  }

  @media (max-width: 768px) {
    .image {
      width: 150px;
      height: 150px;
      /* background-color: #000; */
      border-radius: 50%;
      /* border-color: #fff; */
      overflow: hidden;
      position: absolute;
      /* border-width: 10px 5px 10px 5px; */
      /* border-style: solid; */
      border: solid 4px #f0f0f0;
      border-top-left-radius: 50% 50%;
      border-top-right-radius: 50% 50%;
      border-bottom-right-radius: 50% 50%;
      border-bottom-left-radius: 50% 50%;
      margin-left: 17.5%;
    }
    .image .fotimg{
       position: absolute;
       top: 50%;
       left: 50%;
       transform: translateX(-50%) translateY(-50%);
       max-width: 150%;
       max-height: 100%;
    }
    .butn {
      margin: 129px 0px 0px 74px;
    }
    .butnDos{
      width: 10%;
    }
  }

  @media (min-width: 1200px) {
    .image {
      width: 150px;
      height: 150px;
      /* background-color: #000; */
      border-radius: 50%;
      /* border-color: #fff; */
      overflow: hidden;
      position: absolute;
      /* border-width: 10px 5px 10px 5px; */
      /* border-style: solid; */
      border: solid 4px #f0f0f0;
      border-top-left-radius: 50% 50%;
      border-top-right-radius: 50% 50%;
      border-bottom-right-radius: 50% 50%;
      border-bottom-left-radius: 50% 50%;
      margin-left: 5%;
    }
    .image .fotimg{
       position: absolute;
       top: 50%;
       left: 50%;
       transform: translateX(-50%) translateY(-50%);
       max-width: 150%;
       max-height: 100%;
    }
    .butn {
      margin: 128px 0px 0px 70px;
    }
    .butnDos{
      width: 3%;
    }
  }
    </style>
  </head>
  <body>
    <div class="container"><br>
      <a class="btn btn-primary offset-md-10" href="admin.php" role="button" style="margin-bottom: 5%;">regresar</a>

      <div class="col-md-12">
        <?php
        require("conexion.php");
          $sql2=$mysqli->query("SELECT * FROM fotos WHERE id_solicitante='$id_solicitante'");
            $count= mysqli_num_rows($sql2);
            if ($count==0) {
              $resultFoto="img/user.png";
              } else {
               $sql=$mysqli->query("SELECT f.id_solicitante,f.nombre,s.solicitante,s.carrera FROM solicitantes s JOIN fotos f WHERE f.id_solicitante='$id_solicitante'");
                while($mostrar=mysqli_fetch_array($sql)){
                //$nombre_foto=$mostrar[2].'/'.$mostrar[3].'/'.$mostrar[0].'/'.$mostrar[1];
                $resultFoto=$mostrar[1];
              }
            }
        ?>

        <div class="img-responsive image">
          <img src="<?php echo utf8_encode(utf8_decode($resultFoto)); ?>" class="img-responsive fotimg">
        </div>
        
      <form method="POST" action="" >
        <input type="hidden" name="id_solicitante" id="id_solicitante" value="<?php echo $id_solicitante; ?>">
        <button type="submit" class="btn-success butn"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 50px; border-color:#ffffff00; position: relative;" value="2" id="statusUno" onclick="Validar(document.getElementById('statusUno').value, document.getElementById('id_solicitante').value);"><i class="fa fa-check centered"></i></button>
        <button style="border-radius: 50px; border-color:#ffffff00; position: relative; margin: 0px 0px 0px 10px;"><a download="<?php echo $nombre_foto ?>" href="<?php echo $resultFoto; ?>" download="<?php echo $resultFoto?>" class="fa fa-download btn-link centered"></a></button>
        <button type="submit" class="btn-danger butnDos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 50px; border-color:#ffffff00; position: relative; margin: 0px 0px 0px 8px;" value="1" id="statusCero" onclick="Validar(document.getElementById('statusCero').value, document.getElementById('id_solicitante').value);"><i class="fa fa-times centered"></i></button>    
      </form>
      </div>



      <div class="badge badge-primary text-wrap" style="width: auto; padding: 1em 2em; background-color: #00a48d; margin-top: 2%; margin-bottom: 5%;">
        NOMBRE: <?php $session_sol="SELECT 
                  a.id_archivo,
                  a.id_documento,
                  a.id_solicitante,
                  a.solicitante,
                  a.email,
                  a.archivo,
                  a.ruta_archivo,
                  a.tipo_archivo,
                  a.comentario,
                  a.status,
                  a.date_create,
                  d.nombre,
                  u.id_usuario
                FROM archivos a JOIN documentos d
                ON a.id_documento=d.id_documento
                JOIN usuarios u
                ON d.id_usuario=u.id_usuario 
                where a.id_solicitante = '$id_solicitante';
                
                 ";
              $ressesion=$mysqli->query($session_sol);
              if($sol=mysqli_fetch_array($ressesion)){

                echo $solicitante=$sol[3];
                $id_solicitate=$sol[2];
              } ?><br><br>MATRICULA: <?php echo $id_solicitate ?>
      </div>

      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <!-- <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo"> -->
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          <h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>Confirma, Comenta y Envia</span>
          </h6>
        </div>
      </div>

       <div id="alerta">
            <div class="alert hide" role="alert alert-success" id="alerta">
               <strong id="respuesta"></strong><span id="mensage"></span>
            </div>
        </div>

        <div class="table-responsive">
          <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col" style="text-align: center;">NO.</th>
                  <th scope="col" style="text-align: center;">Documento</th>
                  <th scope="col" style="text-align: center;">Prorroga</th>
                  <th scope="col" style="text-align: center;">Descargar</th>
                  <th scope="col" style="text-align: center;">Status</th>
                  <th scope="col" style="text-align: center;">Comentarios</th>
                  <th scope="col" style="text-align: center;">Fecha</th>  
                </tr>
              </thead>
              <tbody>
                <?php
               $sql="SELECT distinct
                  a.id_archivo,
                  a.id_documento,
                  a.id_solicitante,
                  a.solicitante,
                  a.email,
                  a.archivo,
                  a.ruta_archivo,
                  a.tipo_archivo,
                  a.comentario,
                  a.status,
                  a.date_create,
                  d.nombre,
                  a.prorroga,
                  u.id_usuario
                FROM archivos a JOIN documentos d
                ON a.id_documento=d.id_documento
                JOIN usuarios u
                ON d.id_usuario=u.id_usuario 
                where a.id_solicitante = '$id_solicitante' and a.status!=0 and d.status!=0
                group by  a.id_archivo, a.id_documento, a.id_solicitante
                ORDER BY d.nombre ASC";
              $tipo_archivo="";
              $archivo="No encontrado";
              $ruta_archivo="";
              $query_db=$mysqli->query($sql);
              $num=1;
              while($row=mysqli_fetch_array($query_db)){
                  //$archivo="Descargar";
                  $tipo_archivo=$row[7];
                  $ruta_archivo=$row[6];
                  $nombre_archivo=$row[5];
                switch ($row[9]) {
                  case '1':
                    $status="En proceso...";
                    break;
                  case '2':
                    $status="En prorroga...";
                    break;
                  case '3':
                    $status="Rechazado";
                    break;
                  case '4':
                    $status="Validado";
                    break;
                  default:
                    $status="Lo sentimos este archivo fue desabilitado por el administrador...";
                    break;
                  }
               ?>
                <tr>
                <form class="form-horizontal" id="formArch" method="POST" action="#" onsubmit="return miFuncion(this)" enctype="multipart/form-data">
                <input type="hidden" name="id_archivo[]" value="<?php echo $row[0] ?>" >
                <input type="hidden" name="id_documento[]" value="<?php echo $row[1] ?>" >
                <input type="hidden" name="nombre[]" value="<?php echo $row['nombre'] ?>" >
                <input type="hidden" name="id_solicitante[]" value="<?php echo $row[2]?>">
                <input type="hidden" name="solicitante[]" value="<?php echo utf8_decode($row[3]);?>" >
                <input type="hidden" name="email[]" value="<?php echo $row[4] ?>">
                <input type="hidden" name="archivo[]" value="<?php echo $row[5] ?>">
                <input type="hidden" name="ruta_archivo[]" value="<?php echo $row[6] ?>">
                <input type="hidden" name="tipo_archivo[]" value="<?php echo $row[7] ?>">



                <th scope="row" style="text-align: center;"><?php echo $num; $num++; ?></th>
                <th scope="row" style="text-transform: uppercase; text-align: center;"><?php echo $row[11]?></th>
                <th scope="row" style="text-transform: uppercase; text-align: center;"><?php echo $row[12]?></th>

                <!-- <td><a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>"> <?php echo $archivo?></a></td> -->
                 <!--<td align="center"><a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>" class="fa fa-download"></a></td>-->

                 <td align="center">
                    <?php if($row[12]== 'no'){ ?>
                    <a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>" class="text-dark fa fa-download"></a>
                    <?php } ?>
                </td>

                <td>
                  <select class="form-control" id="" name="status[]">
                    <option value="<?php echo $row[9] ?>" class="bg-success" hidden><?php echo $status; ?></option>
                    
                    <option value="1">En proceso...</option>
                    <option value="2">En prorroga...</option>
                    <option value="3">Rechazado</option>
                    <option value="4">Validado</option>
                  </select>
                </td>

                <td>
                  <input type="text" class="form-control" name="comentario[]" value="<?php echo $row[8]; ?>">
                   <!--<input type="text" class="form-control" name="comentario[]" value="<?php echo $row[8]; ?>" required="">-->
                </td>
                <th>
                  <input type="text" class="form-control" name="date_create[]" value="<?php echo $row[10] ?>" readonly>
              </tr>
                   <?php
                  }
                  ?>
              </tbody>
          </table>
         </div>
         <div class="g-recaptcha" data-sitekey="6LcC2c8ZAAAAALjBme4UlFYxuS08IWXr59OPNvXC" data-callback="enabledSubmit"></div>
         <div id="mensageRecaptcha"></div>
         <input type="submit" class="btn btn-outline-primary offset-md-9" id="btnSubmit">
          <a type="button" class="btn btn-outline-danger" id="btn_cancelar" href="re_archivos.php" >cancelar</a>
      <!--     <input type="submit" class="btn btn-outline-primary btn-block col-md-2 offset-md-9" id="btnSubmit">
          <input type="button" class="btn btn-outline-primary btn-block" id="btn_cancelar" value="cancelar"> -->
        </form>


        <div class="table-responsive" style="margin-bottom: 10%; margin-top: 5%;">
          <h4>Documentos FALTANTES...</h4>
          <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col" style="text-align: center;">NO.</th>
                  <th scope="col" style="text-align: center;">Documento</th>
                  <th scope="col" style="text-align: center;">Descripci&oacute;n</th>
                  <th scope="col" style="text-align: center;">Fecha</th> 
                  <th scope="col" style="text-align: center;">Dependencia</th> 
                </tr>
              </thead>
              <tbody>
                <?php
               $sql="SELECT distinct d.nombre, d.descripcion, d.date_create, d.status
                FROM documentos d 
                JOIN usuarios u
                ON d.id_usuario=u.id_usuario 
                where d.id_documento not in(SELECT id_documento FROM archivos where id_solicitante='$id_solicitante' and status!=0) and d.status=1 or d.status=0 ORDER BY d.nombre ASC";
              $query_db=$mysqli->query($sql);
              $nume=1;
              while($row=mysqli_fetch_array($query_db)){
                switch ($row[3]) {
                  case '0':
                    $status="este archivo fue desabilitado por el administrador...";
                    break;
                  default:
                    $status="el usuario no ha subido este documento";
                    break;
                  }
               ?>
              <tr>
                <form class="form-horizontal" method="POST" action="p_email.php" enctype="multipart/form-data">
                <th style="text-transform: uppercase; text-align: center;"><?php echo $nume; $nume++; ?></th>
                <th style="text-transform: uppercase; text-align: center;"><?php echo $row[0] ?></th>
                <th style="text-transform: uppercase; text-align: center;"><?php echo $row[1] ?></th>
                <th style="text-transform: uppercase; text-align: center;"><?php echo $row[2] ?></td>
                <td>
                  <select class="form-control" id="" name="status">
                    <option selected hidden value="<?php echo $row[3] ?>"><?php echo $status; ?></option>
                  </select>
                </td>
              </tr>
                   <?php
                  }
                  ?>
              </tbody>
            </form>
          </table>
         </div>

  </div>
 

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script type="text/javascript">

    function Validar(status, id_solicitante)
        {
            $.ajax({
                url: 'md_fotos.php',
                type: $(this).attr('method'),
                dataType: 'json',
                data: "status="+status+"&id_solicitante="+id_solicitante,
                success: function(respuesta){
                  if (respuesta.error.error == false) {
                    $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
                      setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 5000);
                    $("#respuesta").html("enviado! ");
                    $("#mensage").html("Se ha notificado al alumno correctamente...");
                  }else if (respuesta.error.error == true){
                    $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-warning").fadeIn();
                      setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-warning").fadeOut(); }, 5000);
                    $("#respuesta").html("Falla! ");
                    $("#mensage").html("Ocurrio un problema con la base de datos");
                  }
                },
                error: function(respuesta){
                $('#btnSubmit').removeAttr('disabled');
                $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-danger").fadeIn();
                    setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-danger").fadeOut(); }, 5000);
                $("#respuesta").html("ERROR! ");
                $("#mensage").html("Hay problemas con el servicio...");
                // $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
                // $("#respuesta").html("error!");
                // $("#mensage").html("el mensage NO ha sido enviado");
              }     
            });
        }


    // $(document).ready(function() {
    //    $("#formArch").bind("submit", function(e) {
    //       e.preventDefault();
    //         $.ajax({
    //           url: $(this).attr('action'),
    //           dataType: 'json',
    //           type: $(this).attr('method'),
    //           data: $(this).serialize(),
    //           beforeSend: function(){
    //             $('#btnSubmit').attr('disabled', 'true');
    //           },
    //           success: function(respuesta) {
    //           if(respuesta.error.error == null) {
    //             $('#btnSubmit').removeAttr('disabled');
    //             $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
    //                setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 1500);
    //             $("#respuesta").html("enviado!");
    //             $("#mensage").html(" se actualizo correctamente, y se notifico al alumno...");
    //           }else if(respuesta.error.error == null){
    //             $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
    //                setTimeout(function(){ $("alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 1500);
    //             $("#respuesta").html("error! ");
    //             $("#mensage").html("el mensage NO ha sido enviado, es necesario escribir un comentario...");
    //           }
    //           },
    //           error: function(respuesta){
    //             $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
    //             $("#respuesta").html("error! ");
    //             $("#mensage").html("el mensage NO ha sido enviado");
    //           }
    //         });
    //         return false;
    //       });
    //     }); 

        $(document).on('submit', '#formArch', function(event){
          event.preventDefault();
          var response = grecaptcha.getResponse();
          var recaptcha = false;

            if(response.length == 0){
                $('#mensageRecaptcha').html('<p class="text-danger center">Captcha no verificado y es obligatorio. *</p>');
                $('#btnSubmit').val('Capturar recaptcha').removeClass('btn btn-outline-primary offset-md-9').addClass('btn btn-outline-warning offset-md-9');
                recaptcha = true;
            } else {
              $('#mensageRecaptcha').html('<p class="text center">Esta pregunta es para comprobar que seas un visitante humano y prevenir envios de spam automatizado.</p>');
            
            if(recaptcha == false){
                $.ajax({
                  dataType: 'json',
                  data: $(this).serialize(),
                  type: $(this).attr("method"),
                //   url: $(this).attr("action"),
                  url: 'p_email.php',
                beforeSend: function(respuesta) {
                    $('#btnSubmit').val('Enviando').removeClass('btn btn-outline-warning offset-md-9').addClass('btn btn-outline-success offset-md-9');
                    $('#btnSubmit').attr('disabled', 'true');
                },
                success: function(respuesta){
                  if (respuesta.error.error == false) {
                    $('#btnSubmit').removeAttr('disabled');
                      $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
                        setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 9000);
                      $("#respuesta").html("enviado! ");
                      $("#mensage").html("se actualizo correctamente, y se notifico al alumno...");
                      window.location.reload();
                    }else if (respuesta.error.error == true){
                      $('#btnSubmit').removeAttr('disabled');
                      $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
                        setTimeout(function(){ $("alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 9000);
                      $("#respuesta").html("error! ");
                      $("#mensage").html("el mensage NO ha sido enviado");
                    }
                  },
                  error: function(respuesta){
                    $('#btnSubmit').removeAttr('disabled');
                    $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
                        setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 9000);
                    $("#respuesta").html("Falla! ");
                    $("#mensage").html("Ocurrio un error al conectarse con el servidor");
                    // $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
                    // $("#respuesta").html("error!");
                    // $("#mensage").html("el mensage NO ha sido enviado");
                  }
                });
            }
            }
            return false;
          }); 
    </script>
  </body>
</html>