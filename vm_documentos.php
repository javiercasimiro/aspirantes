<?php
session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
    require('e_d.php');
    require('conexion.php');
    $id_documento=SED::descryption($_GET['id_documento']);
?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>

    <!-- BEGIN: Content-->
    <div class="container" style="padding-top: 10%;">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-header" style="text-align: center;">
                        <h4 class="card-title" id="basic-layout-form">Editar Documento</h4>
                    </div>
                       <?php
                      $sql="SELECT * from documentos where id_documento='$id_documento'";
                      $result=$mysqli->query($sql);

                      while($mostrar=mysqli_fetch_array($result)){
                      ?>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form name="update-documento" method="POST" action="m_documentos.php">
                              <input type="hidden" id="id_documento" name="id_documento" value="<?php echo $mostrar['id_documento']?>">
                              <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $mostrar['id_usuario']?>">
                                <div class="form-body offset-md-3">
                                   

                                    <div class="row">                                                
                                        <div class="col-md-8">
                                            <div class="form-group">
                                             <label>Nombre de documento</label><br>
                                                <input type="text" id="nombre" class="form-control" 
                                                 name="nombre" value="<?php echo $mostrar['nombre']?>">
                                            </div>
                                        </div>                                                  
                                    </div>


                                      <label>¿Documento obligatorio?</label><br>
                                      <input type="radio" class="op" name="obligatorio" value="si" checked> Si<br>
                                      <input type="radio" class="op" name="obligatorio" value="no"> No<br>

                                      <br>






                                    <div class="row">                                                
                                        <div class="col-md-8">
                                            <div class="form-group">
                                              <label>Descripcion</label><br>
                                                <input type="text" id="descripcion" class="form-control" name="descripcion"
                                                value="<?php echo $mostrar['descripcion']?>">
                                            </div>
                                        </div>                                                  
                                    </div>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="hidden" hidden name="date_create" value="<?php echo $mostrar['date_create']?>">
                                            <input type="hidden" hidden name="status" value="<?php echo $mostrar['status']?>">
                                        </div>
                                    </div> 



                                    <div class="form-actions offset-md-3">
                                        <a class="btn btn-warning" href="re_documentos.php" role="button">Cancelar</a>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-check-square-o"></i> Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                             <?php
                                  }
                                  ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
</body>
<!-- END: Body-->

</html>