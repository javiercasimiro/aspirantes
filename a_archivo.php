<?php
require 'conexion.php';
//sleep(1);

$num_documentos = $_POST['num_documentos'];
$txt_archivos   = '';
$doc_ok         = 0;
$doc_prorroga   = 0;
$doc_error      = 0;
$error          = false;

$permitidos = array("image/jpg", "image/jpeg", "image/png", "image/svg", "text/pdf", "application/pdf");
for($i = 1; $i <= $num_documentos; $i++){
    if($_FILES['archivo'.$i]['name'] != NULL){
        if(!in_array($_FILES['archivo'.$i]['type'], $permitidos)){
            $txt_archivos .= "El archivo subido de: <b>".$_POST['documento'.$i]."</b> tiene un formato no permitido. <br>";
        }
    }
}



if(strlen($txt_archivos) == 0){
    
    $id_solicitante = $_POST["id_solicitante"];
    $solicitante    = $_POST["solicitante"];
    $carrera        = $_POST["carrera"];
    $email          = $_POST["email"];
    $archivo        = "";
    $ruta_archivo   = "";
    $tipo_archivo   = "";
    $comentario     = "";
    $status         = 1;
    date_default_timezone_set('America/Mexico_city');
    $date_create    = date("y-m-d H:i:s");
    $date_update    = null;
    $mensaje        = "";

    for($i = 1; $i <= $num_documentos; $i++){
        
        $id_documento = $_POST['id_documento'.$i];
        
        if(empty($_POST['prorroga'.$i]) && $_FILES['archivo'.$i]['name'] != NULL){     /*Si no pidi�� prorroga pero subi�� archivo*/
            
            $prorroga     = 'no';
            $fechaactual  = date("Y-m-d");  //Fecha Actual
            $ruta_archivo = "archivos/" .$solicitante."_".$id_documento."_".$fechaactual."_".$_FILES['archivo'.$i]['name'];
            
            if (!file_exists($ruta_archivo)){
                $resultado = @move_uploaded_file($_FILES["archivo".$i]["tmp_name"], $ruta_archivo);
                
                if ($resultado){
                
                    $archivo             = $solicitante."_".$id_documento."_".$fechaactual."_".$_FILES['archivo'.$i]['name'];
                    $archivo_extension   = $_FILES["archivo".$i]["name"]; //archivo1.jpg
                    $arrayString         = explode('.', $archivo_extension); //array(archivo1, jpg)
                    $arrayString_archivo = end($arrayString);
                    $tipo_archivo        = '.'.$arrayString_archivo;
                    
                    $mysqli->query(" INSERT INTO archivos VALUES (
                        '',
                        '$id_documento',
                        '$id_solicitante',
                        '$solicitante',
                        '$carrera',
                        '$email',
                        '$prorroga',
                        '$archivo',
                        '$ruta_archivo',
                        '$tipo_archivo',
                        '$comentario',
                        '$status',
                        '$date_create',
                        '$date_update' )");
                    
                 $mail=$mysqli->query("SELECT * FROM usuarios where inf_email='si' and status !=0");
	   	if ($mail->num_rows >= 1) {
	   	    while($mostrar=mysqli_fetch_array($mail)){
	   	    	$mail_usuario=$mostrar['correo_usu'];
	  
	            $carta = "El alumno! $solicitante \r\n";
	            $carta .= "Con MATRICULA: $id_solicitante \r\n";
	            $carta .= "Correo: $email \r\n";
	            
	            $nombredocumento=$mysqli->query("SELECT nombre FROM documentos where id_documento=".$id_documento);
	            if($nombredocumento->num_rows >=1){
	                while($n=mysqli_fetch_array($nombredocumento)){
	                 $carta .= "Ha subido el siguiente documento:" .$n['nombre'] ."\r\n";   
	                }
	            }
	            
	            $carta .= "Fecha de entrega: $date_create \r\n";
	            
	           $destinatario = $mail_usuario;
	            $asunto = utf8_decode("Documentos para admision-".$solicitante);
	            
	            mail($destinatario, utf8_decode($asunto), $carta);
	         
	            
	            
	            $doc_ok++;
	            $error = false;
	   	    }
	    }else{
	    	$doc_ok++;
	    	$error= false;
	    }
    
                    
                    //$carta = "El alumno! $solicitante \r\n";
                    //$carta .= "Con MATRICULA: $id_solicitante \r\n";
                   // ---$carta .= 'javiercasimiro@utsem-morelos.edu.mx';
                    //$carta .= "Correo: $email \r\n";
                    //$carta .= "Ha subido el siguiente documento: $archivo \r\n";
                    //$carta .= "Fecha de entrega: $date_create \r\n";
                    
                    //$destinatario = "javiercasimiro@utsem-morelos.edu.mx";
                   // $asunto .= "Universidad Tecnologica Del Estado De Morelos.";
                    
                    //mail($destinatario, utf8_decode($asunto), $carta);
                    
                   // $doc_ok++;
                   // $error = false;
                    //---$mensaje = "documento(s) ha sido enviado exitosamente...";
                    //----$return['error'] = array('error' => false, 'mensaje' => $mensaje);
                  
                } else {
                    $error = true;
                    //$mensaje = "Ocurrio un error al subir su archivo. Intente mas tarde...";
                    //$return['error'] = array('error' => true, 'mensaje' => $mensaje);
                }
            } else {
                $error = true;
                //$mensaje = $_FILES['archivo'.$i]['name'] . ", este archivo ya existe en nuestro servidor...";
                //$return['error'] = array('error' => true, 'mensaje' => $mensaje);
            }
            
        }elseif(!empty($_POST['prorroga'.$i]) && $_FILES['archivo'.$i]['name'] == NULL){ /*Si pidi�� prorrogra y no subi�� documento*/
            
            $prorroga = 'si';
            $resultado = $mysqli->query(" INSERT INTO archivos VALUES (
              '',
              '$id_documento',
              '$id_solicitante',
              '$solicitante',
              '$carrera',
              '$email',
              '$prorroga',
              '$archivo',
              '$ruta_archivo',
              '$tipo_archivo',
              '$comentario',
              '$status',
              '$date_create',
              '$date_update' )");
              
             if($resultado){
                 
                 $mail=$mysqli->query("SELECT * FROM usuarios where inf_email='si' and status !=0");
	   	if ($mail->num_rows >= 1) {
	   	    while($mostrar=mysqli_fetch_array($mail)){
	   	    	$mail_usuario=$mostrar['correo_usu'];
	  
	            $carta = "El alumno! $solicitante \r\n";
	            $carta .= "Con MATRICULA: $id_solicitante \r\n";
	            $carta .= "Correo: $email \r\n";
	            
	            $nombredocumento=$mysqli->query("SELECT nombre FROM documentos where id_documento=".$id_documento);
	            if($nombredocumento->num_rows >=1){
	                while($n=mysqli_fetch_array($nombredocumento)){
	                 $carta .= "Ha subido el siguiente documento en prorroga:" .$n['nombre'] ."\r\n";   
	                }
	            }
	            
	            $carta .= "Fecha de entrega: $date_create \r\n";
	            
	           $destinatario = $mail_usuario;
	            $asunto = utf8_decode("Documentos para admision-".$solicitante);
	            
	            mail($destinatario, utf8_decode($asunto), $carta);}}
                 
                 $doc_prorroga++;
                 $error = false;
                //$mensaje= "Documento(s) han sido enviados con prorroga...";
                //$return['error'] = array('error' => false, 'mensaje' => $mensaje);
             }else{
                 
                 $error = true;
                 
             }
            
        }elseif(empty($_POST['prorroga'.$i]) && $_FILES['archivo'.$i]['name'] == NULL){ /*Ni pidi�� prorroga ni subi�� archivo*/
            
            $doc_error++;
            
        }else{
            
            $error = false;
            
        }
    $tipo_archivo ='';    
    $archivo = '';   
    $ruta_archivo = '';
    
    }
    
    if($error == FALSE){
        
        $mensaje = ($doc_ok > 0 ? $doc_ok.' archivos(s) subidos correctamente <br>' : '');
        $mensaje .= ($doc_prorroga > 0 ? $doc_prorroga.' archivos(s) subidos con prorroga <br>' : '');
        $mensaje .= ($doc_error > 0 ? $doc_error.' Seleccionar las casillas para pedir prorroga <br>' : '');
        //$mensaje .= ($doc_error > 0 ? $doc_error.' archivo(s) no subidos por que no pidio prorroga ni selecciono un archivo <br>' : '');
        $return = array('error' => false, 'mensaje' => $mensaje);
        
    }else{
        
        $mensaje = "Hubo un error durante el proceso. Intente mas tarde";
        $return = array('error' => true, 'mensaje' => $mensaje);
        
    }
    
}else{
    $mensaje = $txt_archivos."Estos son los permitidos: <br>".implode(',', $permitidos);
    $return = array('error' => true, 'mensaje' => $mensaje);
}


/*$id_solicitante=$_POST["id_solicitante"];
$solicitante=$_POST["solicitante"];
$carrera=$_POST["carrera"];
$email=$_POST["email"];
$archivo=$_POST["archivo"];
$ruta_archivo='';
$tipo_archivo='';
$comentario = "";
$prorroga=(empty($_POST["prorroga"])?"no":$_POST["prorroga"]);
$status = 1;
date_default_timezone_set('America/Mexico_city');
$date_create=date("y-m-d H:i:s");
$date_update=null;
$mensaje = "";

require("conexion.php");

foreach (array_keys($_POST['id_documento']) as $key) {
  $id_documento = $_POST['id_documento'][$key];

   if ($_FILES['archivo']['name']!= null){
        if ($_FILES["archivo"]["error"][$key] >= 1){
        $mensaje='Se ha subido el documento '.$archivo;
        $return['error'] = array('error' => false, 'mensaje' => $mensaje);
        } 
        
        
        else {
            $permitidos = array("image/jpg", "image/jpeg", "image/png", "image/svg", "text/pdf", "application/pdf");
            if (in_array($_FILES['archivo']['type'][$key], $permitidos)){
                $fechaactual  = date("Y-m-d");  //Fecha Actual
                $ruta_archivo = "archivos/" .$solicitante."_".$id_documento."_".$fechaactual."_".$_FILES['archivo']['name'][$key];
                if (!file_exists($ruta_archivo)){
                    $resultado = @move_uploaded_file($_FILES["archivo"]["tmp_name"][$key], $ruta_archivo);
                    if ($resultado){
                    
                    $archivo = $solicitante."_".$id_documento."_".$fechaactual."_".$_FILES['archivo']['name'][$key];
                    $archivo_extension = $_FILES["archivo"]["name"][$key]; //archivo1.jpg
                    $arrayString = explode('.', $archivo_extension); //array(archivo1, jpg)
                    $arrayString_archivo = end($arrayString);
                    $tipo_archivo = '.'.$arrayString_archivo;
                    $mysqli->query("
                        INSERT INTO archivos
                        VALUES (
                        '',
                        '$id_documento',
                        '$id_solicitante',
                        '$solicitante',
                        '$carrera',
                        '$email',
                        '$prorroga',
                        '$archivo',
                        '$ruta_archivo',
                        '$tipo_archivo',
                        '$comentario',
                        '$status',
                        '$date_create',
                        '$date_update'
                        )");
                    
                    $carta = "El alumno! $solicitante \r\n";
                    $carta .= "Con MATRICULA: $id_solicitante \r\n";
                    $carta .= 'javiercasimiro@utsem-morelos.edu.mx';
                    //$carta .= "Correo: $email \r\n";
                    $carta .= "Ha subido el siguiente documento: $archivo \r\n";
                    $carta .= "Fecha de entrega: $date_create \r\n";
                    
                    $destinatario = "";
                    $asunto .= "Universidad Tecnologica Del Estado De Morelos.";
                    
                    mail($destinatario, utf8_decode($asunto), $carta);
                    
                    $mensaje= "documento(s) ha sido enviado exitosamente...";
                    $return['error'] = array('error' => false, 'mensaje' => $mensaje);
                  
                } else {
                $mensaje= "ocurrio un error al enviar su archivo. INTENTE mas tarde...";
                $return['error'] = array('error' => true, 'mensaje' => $mensaje);
                }
            } else {
              $mensaje= $_FILES['archivo']['name'][$key] . ", este archivo ya existe en nuestro servidor...";
              $return['error'] = array('error' => true, 'mensaje' => $mensaje);
            }
        } else {
            $mensaje= $_FILES['archivo']['name'][$key] . ", archivo no permitido, este tipo de archivo es prohibido solo son permitidos: " . implode(',', $permitidos). " Gracias por tu complención";
            $return['error'] = array('error' => true, 'mensaje' => $mensaje);
        }
    }
       
   }
    
    
    
    
    else if ($_FILES['archivo']['name']== null && $prorroga=='si'){
        $mysqli->query("
            INSERT INTO archivos
            VALUES (
              '',
              '$id_documento',
              '$id_solicitante',
              '$solicitante',
              '$carrera',
              '$email',
              '$prorroga',
              '$archivo',
              '$ruta_archivo',
              '$tipo_archivo',
              '$comentario',
              '$status',
              '$date_create',
              '$date_update'
            )");
        $mensaje= "documento(s) han sido enviados con prorroga...";
        $return['error'] = array('error' => false, 'mensaje' => $mensaje);
    }else if ($_FILES['archivo']['name']== null && $prorroga=='no'){
        //$mensaje='Antes tienes que dar click en "ENTREGAR DESPUES lOS DOCUMENTOS" esto para poder solicitar prorroga...';
        //$return['error'] = array('error' => true, 'mensaje' => $mensaje);
    }
    
    
    
}*/

header("Content-Type: application/json");
echo json_encode($return);
?>
