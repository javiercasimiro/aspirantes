<?php

session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }





?>


<!DOCTYPE html>
<html lang="en">
  <head><meta charset="gb18030">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
	<title>UTSEM</title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="librerias_buscar/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="librerias_buscar/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="librerias_buscar/css/buttons.dataTables.min.css">
</head>
<body>
    
    <header>
        <!-- <h1 class="text-center text-light">Tutorial</h1>-->
         <!--<h2 class="text-center text-light">Cómo usar <span class="badge badge-danger">DATATABLES</span></h2> -->
     </header> 
    <div style="height:50px"></div>
    
    <!--Ejemplo tabla con DataTables-->
    <div class="container">
        <a class="btn btn-primary offset-md-10" href="admin.php" role="button">regresar</a>
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:90px;" alt="branding logo">
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          
        
		<br><br>
		<h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>Revisi&oacute;n de documentos</span>
          </h6>
        </div>         
      </div>
        
        
       <div class="row">
           <div class="col-lg-12">
           <div class="table-responsive"> 
           
           
           

  <table id="tablaArchivos" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <th>No</th>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>PE</th>
                    <th>Fecha pr&oacute;rroga</th>
                    <th>Fecha Modificaci&oacute;n</th>
                    <th>Fecha creaci&oacute;n</th>
                    <th>Status</th>
                </thead>
                
        
        
        
        <tbody>
                    
        
        
        <?php
        require("e_d.php");
        require("conexion.php");
        $query="SELECT distinct a.id_archivo,
                  a.id_documento,
                  a.id_solicitante,
                  a.carrera,
                  a.solicitante,
                  a.email,
                  a.archivo,
                  a.ruta_archivo,
                  a.tipo_archivo,
                  a.comentario,
                  a.status,
                  a.date_create,
                  a.date_update,
                  d.nombre,
                  d.id_documento,
                 
                  (SELECT fecha_entrega FROM prorroga WHERE id_solicitante=a.id_solicitante)AS fecha_prorroga,
                  
                  u.id_usuario FROM archivos a
                  
                  JOIN documentos d
                  ON a.id_documento = d.id_documento
                  
                  JOIN usuarios u
                  ON d.id_usuario = u.id_usuario
                  
       
                  
                  group by a.id_solicitante ORDER BY a.date_create asc";

                  
            $query_db=$mysqli->query($query);
            $number=1;
              
            while($row=mysqli_fetch_array($query_db)){
                $consulta="SELECT id_archivo, id_documento, status FROM archivos WHERE id_solicitante='$row[2]'";
                $resultado=$mysqli->query($consulta);
                $array_status=array();
                $status_general="";
                $bg="";
                while($row2=mysqli_fetch_array($resultado)){
                $array_status[]=$row2[2]; 
                 //array_push($row2[2],$array_status);
                }
                
                $consulta2="SELECT status FROM fotos WHERE id_solicitante='$row[2]'";
                $resultado2=$mysqli->query($consulta2);
               while($row3=mysqli_fetch_array($resultado2)){
                   $status_fotos=($row3[0]==2?4:1);
                   $array_status[]=$status_fotos;
                   //array_push($status_fotos, $array_status);
                }
                
                //for($i=0;$i<count($array_status);$i++){
                    //echo '$array_status[$i]';
                //}
                echo print_r ($array_status);
                
                if(array_sum($array_status)== count($array_status)){
                    $status_general="Documentos sin revisar";
                    $bg="secundary";
                }
               else if (array_sum($array_status)== (count($array_status) * 4)){
                   $status_general="Documentos validados";
                   $bg="success";
                 
                   
               }
               
               else if (in_array(2,$array_status) ){
                  if(in_array(3,$array_status) or in_array(1,$array_status)){
                       $status_general="Documentos en proceso";
                       $bg="warning";
                  } else{ $status_general="Documentos en prorroga";
                      $bg="danger";
                  }
                  
               }
               
               else if(in_array(3,$array_status) or in_array(1,$array_status)){
                   $status_general="Documentos en proceso";
                    $bg="warning";
                   
               }
               else{ $status_general="Error de Documentos";
                   
               }
           
           
        ?>
  
                    <tr>
                        <td><?php echo $number; $number++;?></td>
                        <td><?php echo $row['id_solicitante']?></td>
                        <td><?php echo $row['solicitante']?></td>
                        <td><?php echo $row['carrera']?></td>
                        <td><?php $row['fecha_prorroga']==''? print_r('Ningun resultado'): print_r($row['fecha_prorroga']) ?></td>
                        <td><?php echo $row['date_update']?></td>
                        <td><?php echo $row['date_create']?></td>
                        <!-- <td><a href="vm_archivos.php">Status</a></td>-->
                        <td class="center"style="text-align: center;"><a class="text-<?php echo $bg; ?>" href="vm_archivos.php?id_solicitante=<?php echo SED::encryption($row["id_solicitante"])?>"><?php echo $status_general;?></a></td>
                    </tr>
           <?php  }?>
                    
                    
                  
                </tbody>
                
                
        <tfoot>
            <tr>
                    <th>No</th>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>PE</th>
                     <th>Fecha pr&oacute;rroga</th>
                    <th>Fecha Modificaci&oacute;n</th>
                    <th>Fecha creaci&oacute;n</th>
                    <th>Status</th>
            </tr>
        </tfoot>
    </table>
    
          </div>
       </div> 
    </div>
    </div> 

    <!--Online
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>-->



<script src="librerias_buscar/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="librerias_buscar/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="librerias_buscar/js/dataTables.bootstrap4.min.js"></script>


<script type="text/javascript" src="librerias_buscar/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="librerias_buscar/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="librerias_buscar/js/jszip.min.js"></script>
<script type="text/javascript" src="librerias_buscar/js/pdfmake.min.js"></script>
<script type="text/javascript" src="librerias_buscar/js/vfs_fonts.js"></script>
<script type="text/javascript" src="librerias_buscar/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="librerias_buscar/js/buttons.print.min.js"></script>





<script type="text/javascript">
	$(document).ready( function () {
    $('#tablaArchivos').DataTable({
    
"language": {
      //"url": src="https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }

    },



     dom: 'Blfrtip', //sib l se quitar en ver por cantidad de datos
     // "lengthChange": false, //para ocultar cantidad de datos
     "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    


    buttons: [
        'excel', 'pdf'
    ],




   "order": [[ 4, 'des' ]]


    
});


});


</script>

 <div class="col-md-2 offset-md-5">
        <a class="btn btn-outline-primary btn-block" href="admin.php" role="button">Cancelar</a>
        </div><br><br>
      </div>

</body>
</html>