<?
include('conexion.php');//CONEXION A LA BD

$fecha1=$_POST['fecha1'];
$fecha2=$_POST['fecha2'];

if(isset($_POST['generar_reporte']))
{
	// NOMBRE DEL ARCHIVO Y CHARSET
	header('Content-Type:text/csv; charset=latin1');
	header('Content-Disposition: attachment; filename="Reporte_Fechas_Ingreso.csv"');

	// SALIDA DEL ARCHIVO
	$salida=fopen('php://output', 'w');
	
	// ENCABEZADOS
	fputcsv($salida, array('Matricula', 'Nombre', 'Carrera', 'Nombre documento', 'Fecha de creacion'));
	
	// QUERY PARA CREAR EL REPORTE
	$reporteCsv=$mysqli->query("SELECT DISTINCT a.id_solicitante, a.solicitante, a.carrera, d.nombre, a.date_create FROM archivos a INNER JOIN documentos d ON a.id_documento=d.id_documento where  a.status=3 and a.date_create BETWEEN '$fecha1' AND '$fecha2' ORDER BY a.id_archivo");
	while($filaR= $reporteCsv->fetch_assoc())
		fputcsv($salida, array($filaR['id_solicitante'], 
								$filaR['solicitante'],
								$filaR['carrera'],
								$filaR['nombre'],
								$filaR['date_create']));
}
?>