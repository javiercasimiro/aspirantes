<?php
session_start();
if(isset($_SESSION['id_usuario'])){
  if ($_SESSION['status'] == '0') {
    header("location: admin.php");
  }else if ($_SESSION['status'] >= '1') {
    header('location: admin.php');
  }
}
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <title>UTSEM</title>
  </head>
  <body>
  <div class="col-12 d-flex align-items-center justify-content-center">
    <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0"><br><br>
      <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
        <div class="card-header border-0" style="background-color: white;">
          <div class="card-title text-center">
            <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo">
          </div>         
        </div>
      <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          <h5 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
            <span>Bienvenido</span>
          </h5><br>
        <div class="card-content" id="userFormContainer">
          <div class="card-body" style="padding-top: 0;">
            <a class="btn btn-outline-primary btn-block" href="l-admin.php" role="button">Administrador</a>
            <a class="btn btn-outline-primary btn-block" href="l-usuario.php" role="button">Usuario</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>