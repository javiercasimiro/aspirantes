<?php
session_start();
if ($_SESSION['solicitante'] == '') {
  header('Location: l-usuario.php');

}
$id_usuario=1;
$matricula =$_SESSION['solicitante']['matricula'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <style type="text/css">
      .barra{
        background-color: #f3f3f3;
        border-radius: 25px;
        box-shadow: inset 0px 0px 0px rgba(0,0,0,.2);
        height: 10px; 
      }
      .barra_azul{
        background-color: #247CC0;
        border-radius: 10px;
        display: block;
        height: 25px;
        line-height: center;
        width: 0%;
      }
      .barra_verde{
        background-color: #2EA265 !important;
      }
      .barra_roja{
        background-color: #DE3152 !important;
      }
      #barra_estado span {
        color: #FFF;
        font-weight: bold;
        line-height: 25px;
      }
    </style>
    <title>UTSEM</title>
  </head>
  <body>
    <div class="container"><br>
      <a type="submit" class="col-md-1 offset-md-10 btn btn-outline-primary btn-block" href="desconectar.php" role="button" style="margin-bottom: 5%;">Salir</a>
      <div class="badge badge-primary text-wrap" style="width: auto; padding: 1em 2em; background-color: #00a48d;">
        MATRICULA: <?php echo $_SESSION['solicitante']['matricula']; ?>
      </div>
      
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo">
        </div>
      </div>
      <h4 style="text-align: center;">Bienvenid@ <?php echo $_SESSION['solicitante']['Nombre'];?></h4>
      <h3 style="text-align: center;">Sistema de Admisión Online </h3>

      	<div id="alerta">
            <div class="alert hide" role="alert alert-success" id="alerta">
               <strong id="respuesta"></strong><span id="mensage"></span>
            </div>
        </div>

        <?php
              require("conexion.php");
              // $sql1="SELECT * FROM documentos d JOIN archivos a
              // ON d.id_documento=a.id_documento 
              // JOIN usuarios u
              // ON u.id_usuario=d.id_usuario
              // where d.id_documento not in(SELECT id_documento FROM archivos where id_solicitante='$matricula')";
              $sql1="SELECT * FROM documentos d JOIN usuarios u WHERE d.id_documento not in(SELECT id_documento FROM archivos WHERE id_solicitante = '$matricula' and status!=0) and d.status=1 ORDER BY d.nombre ASC";
              $result1=$mysqli->query($sql1);
              if($mostrar1=mysqli_fetch_array($result1)){
              ?>
        <h4 style="text-align: center;">Documentos por entregar</h4>
        <!-- <form class="form-horizontal" id="form_a_archivo" method="POST" action="a_archivo.php" enctype="multipart/form-data"> -->
       	  <div class="table-responsive">
	        <table class="table table-hover">

	            <thead>
	              <tr>
	                <th scope="col">Documento</th>
	                <th scope="col">Archivo</th>
	              </tr>
	            </thead>
	            <tbody>
	              <?php
	              require("conexion.php");
	              $sql="SELECT * FROM documentos d 
	              JOIN usuarios u
	              ON d.id_usuario=u.id_usuario 
	              where d.id_documento not in(SELECT id_documento FROM archivos where id_solicitante='$matricula' and status!=0) and d.status=1 ORDER BY d.nombre ASC";
	              $result=$mysqli->query($sql);
	              while($mostrar=mysqli_fetch_array($result)){
	              ?>
	              <tr>
                  <form class="form-horizontal" id="form_a_archivo" method="POST" action="a_archivo.php" enctype="multipart/form-data">
	                <th scope="row" style="text-transform: uppercase;"><?php echo $mostrar['nombre']?></th>
	                <input type="hidden" name="id_solicitante" value="<?php echo $_SESSION['solicitante']['matricula']; ?>">
	                <input type="hidden" name="solicitante" value="<?php echo $_SESSION['solicitante']['Nombre']; ?>">
	                <input type="hidden" name="email" value="<?php echo $_SESSION['solicitante']['mail']; ?>">
	                <input type="text" name="id_documento[]" value="<?php echo $mostrar['id_documento'] ?>" hidden="">
	                <td><input type="file" class="form-control-file" accept="image/*,.pdf" name="archivo[]" required=""></td>
	                 <!-- <td><input type="file" class="form-control-file" accept=".pdf,.jpg,.png" id="id_archivo" name="imagen[]"  required=""></td> -->
	              </tr>
	                 <?php
	                }
	                ?>
	            </tbody>
	        </table>
         </div>
        <!-- <div class="barra">
          <div class="barra_azul" id="barra_estado">
            <span></span>
          </div>
        </div> -->
        <div class="col-md-4 offset-md-4">
        	<input type="submit" class="btn btn-outline-primary btn-block" id="btnSubmit">
          <!-- <input type="button" class="btn btn-outline-primary btn-block" id="btn_cancelar" value="cancelar"> -->
        </div>
    </form>
    <?php } ?>
    
    	<h4 style="text-align: center; margin-top: 7%;">Documentos entregados</h4>
    	<div class="table-responsive">
          <table class="table table-hover">
            
            <thead>
              <tr>
                <th scope="col">Documento</th>
                <th scope="col">Descargar</th>
                <th scope="col">Archivo</th>
                <th scope="col">Status</th>
                <th scope="col" style="text-align: center;">Comentarios</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              require("conexion.php");
              $sql="SELECT * FROM documentos d 
              JOIN archivos a
              ON d.id_documento=a.id_documento 
              where a.id_solicitante in(SELECT id_solicitante FROM archivos where id_solicitante='$matricula') and d.status=1 and a.status!=0 ORDER BY d.nombre ASC";

              $result=$mysqli->query($sql);
              while($mostrar=mysqli_fetch_array($result)){
              	$tipo_archivo=$mostrar['tipo_archivo'];
                $ruta_archivo=$mostrar['ruta_archivo'];
                $nombre_archivo=$mostrar['archivo'];
              ?>
              <tr>
                <th scope="row" style="text-transform: uppercase;"><?php echo $mostrar['nombre']?></th>

                <td align="center"><a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>" class="text-dark fa fa-download"></a></td>

                <td><span>Enviado Correctamente</span></td>
                 <!-- <td><input type="file" class="form-control-file" accept=".pdf,.jpg,.png" id="id_archivo" name="imagen[]"  required=""></td> -->
                <?php
                switch ($mostrar['status']) {
                  case '1':
                    $estado="en proceso";
                    break;
                  case '2':
                    $estado="Rechazado";
                    break;
                  case '3':
                    $estado="Validado";
                    break;
                  default:
                    $estado="Lo sentimos este archivo fue desabilitado por el administrador..";
                    break;
                }
                 ?>
                <td><?php echo  $estado?></td>
                <td style="text-align: center;"><?php echo $mostrar['comentario']?>
                </td>
              
              <td>
              <form id="e_archivos_solicitante" method="POST" action="e_archivos_solicitante.php">
                <input type="hidden" name="id_archivo" value="<?php echo $mostrar['id_archivo'] ?>">
                <button type="submit" name="id_archivo" id="archivoEli">
                <i class="fa fa-trash-alt centered"></i>
              </form>
              </td>
            </tr>
                 <?php
                }
                ?>
            </tbody>
          </table>
      	</div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="r_u.js"></script>
<script type="text/javascript">
$(document).on('submit', '#e_archivos_solicitante', function(event){
  
  event.preventDefault();

  $.ajax({
    url: $(this).attr('action'),
    type: $(this).attr('method'),
    dataType: 'json',
    data: $(this).serialize(),
    beforeSend: function(){
      $('#archivoEli').attr('disabled', 'true');
    }
  })

  .done(function(respuesta){
    if (!respuesta.error) {
      if (respuesta.tipo == 1 || respuesta.tipo == 2) {
      	$('#archivoEli').removeAttr('disabled');
        $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
        setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 1000);
        $("#respuesta").html("Hecho!");
        $("#mensage").html(" se elimino correctamente...");
        document.location.href='user-form.php';
      }else if (respuesta.tipo == 3) {
        $('#archivoEli').removeAttr('disabled');
        $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
        setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 2000);
        $("#respuesta").html("error! ");
        $("#mensage").html("NO ha se apodido eliminar, por que ya fue validado...");
      }

    }else{
    	  $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
    		 setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 2000);
    	  $("#respuesta").html("OCURRIO UN ERROR! ");
        $("#mensage").html("NO ha se apodido eliminar, los datos no llegan al servidor...");
        document.location.href='user-form.php'; 
    }
  })
  .fail(function(respuesta){
  })
  .done(function(){
  });
});


    $(document).ready(function() {
       $("#form_a_archivo").on("submit", function(event) {
          event.preventDefault();
          var formData = new FormData(this);

            $.ajax({
              url: $(this).attr('action'),        
              type: $(this).attr('method'),
              dataType: 'json',
              data: formData,
              contentType: false,
              processData: false,
              cache: false,
              beforeSend: function(){
                $('#btnSubmit').val('subiendo...');
              },
              success: function(respuesta) {
              if(respuesta.error.error == false) {
                $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
                setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 10000);
                $("#mensage").html(respuesta.error.mensaje);
                location.href='user-form.php';
              } else if(respuesta.error.error == true){
                $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
                setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 3000);
                $("#mensage").html(respuesta.error.mensaje);
                $('#btnSubmit').val('reenviar');
              }
              },
              error: function(respuesta){
                $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
                $("#respuesta").html("error! ");
                $("#mensage").html("NO ha sido posible comunicarse con el servidor...");
              }
            });
            return false;
          });
        });
</script>
</body>
</html>