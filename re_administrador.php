<?php
require("e_d.php");
require("conexion.php");
session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>
  <body>
    <div class="container"><br>
      <a class="btn btn-primary offset-md-10" href="admin.php" role="button">regresar</a>
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:90px;" alt="branding logo">
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          <h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>Gestión de Usuarios</span>
          </h6><br><br>
        </div>         
      </div>
    <!-- BEGIN: Content-->

<div class="row">
    <div class="col-md-6">

      <div class="app-content content">
        <div class="content-wrapper">
            <div class="content">
              <div class="content-body">
                <section class="row all-contacts">
                  <div class="col-md">
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body">

                        <div class="card-body">
                            <form name="registro-config" method="POST" action="a_administrativos.php">
                                <div class="form-body">
                                    <div class="row">                                                
                                        <div class="col-md-12">
                                            <div class="form-group">
                                              <label>Nombre completo:</label>
                                                <input type="text" id="nombre_usu" class="form-control" name="nombre_usu" placeholder="Nombre del usuario"  required>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">                                                
                                        <div class="col-md-12">
                                            <div class="form-group">
                                              <label>Correo electr&oacutenico:</label>
                                                <input type="email" id="correo_usu" class="form-control" name="correo_usu" placeholder="Correo electronico" required>
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div class="row">  
                                        <div class="col-md-12">
                                          <label>Contrase&ntildea:</label>
                                            <div class="input-group mb-3">
                                             <input name="password_usu" type="password" class="form-control" id="password_usu" placeholder="" required data-container="body" data-toggle="popover" data-trigger="focus" data-placement="left" data-content="Ingresa tu contraseña correctamente.">

                                            <div class="input-group-append">
                                              <span class="btn btn-outline-primary btn-block" onclick="mostrarContrasena()"><i id="icon" class="fa fa-eye fa-1x"></i></span>
                                            </div>
                                          </div>                             
                                        </div>                                                 
                                    </div>



                                    <!-- <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Periodo desde:</label>
                                            <input type="date" id="periodo_ini" class="form-control" name="periodo_ini" required>
                                        </div>                                 
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Hasta:</label>
                                            <input type="date" id="periodo_fin" class="form-control" name="periodo_fin" required>
                                        </div>                                     
                                      </div>
                                    </div> -->


                                   <!-- <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-check">
                                          <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                          <label class="form-check-label" for="defaultCheck1">
                                            Enviar SMS
                                          </label>
                                            <input type="text" id="inf_sms" class="form-control" name="inf_sms">                 
                                        </div>
                                      </div> -->


                                      <div class="col-md">
                                        <div class="form-check">
                                          <input class="form-check-input" type="checkbox" name="inf_email" value="si" id="defaultCheck2">
                                          <label class="form-check-label" for="defaultCheck2">
                                            Recibir notificaciones por E-mail de los archivos enviados por los alumnos?
                                          </label>
                                          
                                        </div>
                                      </div>



                                    </div><br><br>
                                    <input type="hidden" name="status" value="<?php echo $_SESSION['id_usuario']?>">
                                    <div class="form-actions offset-md-7">
                                        <a class="btn btn-warning" href="admin.php" role="button">Cancelar</a>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-check-square-o"></i> Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>    



                                     
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
    </div><br><br>

       <section class="row all-contacts">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-head">
                        <div class="card-header">
                          <h4>Almacenados...</h4>
                        </div>
                      </div>
                      <div class="card-content">
                        <div class="card-body">
                        <!-- Task List table -->
                          <div class="table-responsive">
                            <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle text-center">
                            <thead style="background-color:white;">
                              <tr>
                                <th>Usuario</th>
                                <th>Correo</th>
                                <th>Creación</th>
                                <th>Visualizar</th>
                                <!-- <th>eliminar</th> -->
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              $sql="SELECT * FROM usuarios WHERE status=0";
                              // SELECT * FROM documentos d join usuarios u
                              // ON d.id_usuario=u.id_usuario WHERE d.id_usuario='$id_usuario' and d.status=0
                              $result=$mysqli->query($sql);
                              while($mostrar=mysqli_fetch_array($result)){
                              ?>

                              <tr>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['nombre_usu']?></a>
                                  </div>
                                </td>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['correo_usu']?></a>
                                  </div>
                                </td>
                                 <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['date_create']?></a>
                                  </div>
                                </td>
                                <td>
                                  <form name="eliminar-documento" method="POST" action="md_usuarios.php" >
                                    <button type="submit" name="id_usuario" value="<?php echo $mostrar['id_usuario']?>" class="fa fa-reply-all btn btn-dark">
                                  </form>
                                </td>                                                       
                              </tr>
                                 <?php
                                }
                                ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
    </div>
    <div class="col-12 col-md-6">

      <div class="app-content content">
        <div class="content-wrapper">
            <div class="content">
              <div class="content-body">
                <section class="row all-contacts">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-head">
                        <!-- <div class="card-header">
                          <a class="btn btn-primary offset-md-10" href="r_administrativos.php" role="button">Agregar nuevo usuario</a>
                        </div> -->
                      </div>
                      <div class="card-content">
                        <div class="card-body">
                        <!-- Task List table -->
                          <div class="table-responsive">
                            <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle text-center">
                            <thead style="background-color:white;">
                              <tr>
                                <th>Usuarios</th>
                                <th>Correo</th>
                                <th>Recibir correos</th>
                                <th>Fecha de creación</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                                <th>Archivar</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php

                              $sql="SELECT * FROM usuarios WHERE status>=1 ORDER BY nombre_usu ASC";
                              $result=$mysqli->query($sql);                              
                              while($mostrar=mysqli_fetch_array($result)){                             
                              ?>
                              <tr>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['nombre_usu']?></a>
                                  </div>
                                </td>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['correo_usu']?></a>
                                  </div>
                                </td>
                                  <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['inf_email']?></a>
                                  </div>
                                </td>
                                <td>
                                <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['date_create']?></a>
                                  </div>
                                </td>
                                <td>
                                  <form action="vm_usuarios.php">
                                      <button type="submit" name="id_usuario" value="<?php echo SED::encryption($mostrar['id_usuario'])?>" class="fa fa-wrench btn btn-warning">
                                  </form>
                                </td>
                                <td>
                                  <form name="eliminar-documento" method="POST" action="e_usuarios.php">
                                    <button type="submit" name="id_usuario"  value="<?php echo $mostrar['id_usuario']?>" class="fa fa-times btn btn-danger">
                                  </form>
                                </td>


                                <td>
                                  <form name="eliminar-documento" method="POST" action="ma_usuarios.php">
                                    <button type="submit" name="id_usuario"  value="<?php echo $mostrar['id_usuario']?>" class="fa fa-folder btn btn-light">
                                  </form>
                                </td> 



                              </tr>
                                 <?php
                                }
                                ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section><br><br>

             
            </div>
          </div>
        </div>
    </div><br><br>
  </div>
</div>
    <!-- END: Content-->
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript">
      function mostrarContrasena(){
      var transform = document.getElementById("password_usu");
      if(transform.type == "password"){
          transform.type = "text";
          $('#icon').removeClass('fa fa-eye fa-1x').addClass('fa fa-eye-slash fa-1x');
      }else{
          transform.type = "password";
          $('#icon').removeClass('fa fa-eye-slash fa-1x').addClass('fa fa-eye fa-1x');
      }
}
    </script>
  </body>
</html>