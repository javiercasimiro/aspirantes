<?php
session_start();
require("conexion.php");
if ($_SESSION['solicitante'] == '') {
  header('Location: l-usuario.php');
}
$id_usuario=1;
$matricula =$_SESSION['solicitante']['matricula'];
require("e_d.php");

$consulta_foto="SELECT * FROM fotos WHERE id_solicitante='$matricula'";
$resultado_consulta_foto=$mysqli->query($consulta_foto);
$rows =mysqli_num_rows($resultado_consulta_foto);
if( $rows == 0){
    header('Location: user-u-image.php');
}

?>


<!doctype html>
<html lang="en">
  <head><meta charset="gb18030">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->

<style type="text/css">
	@media (min-width: 576px) {
		.image {
      width: 150px;
      height: 150px;
      /* background-color: #000; */
      border-radius: 50%;
      /* border-color: #fff; */
      overflow: hidden;
      position: absolute;
      /* border-width: 10px 5px 10px 5px; */
      /* border-style: solid; */
      border: solid 4px #f0f0f0;
      border-top-left-radius: 50% 50%;
      border-top-right-radius: 50% 50%;
      border-bottom-right-radius: 50% 50%;
      border-bottom-left-radius: 50% 50%;
    }
    .image .fotimg{
       position: absolute;
       top: 50%;
       left: 50%;
       transform: translateX(-50%) translateY(-50%);
       max-width: 150%;
       max-height: 100%;
    }
	}

	@media (max-width: 768px) {
    .image {
      width: 150px;
      height: 150px;
      /* background-color: #000; */
      border-radius: 50%;
      /* border-color: #fff; */
      overflow: hidden;
      position: absolute;
      /* border-width: 10px 5px 10px 5px; */
      /* border-style: solid; */
      border: solid 4px #f0f0f0;
      border-top-left-radius: 50% 50%;
      border-top-right-radius: 50% 50%;
      border-bottom-right-radius: 50% 50%;
      border-bottom-left-radius: 50% 50%;
    }
    .image .fotimg{
       position: absolute;
       top: 50%;
       left: 50%;
       transform: translateX(-50%) translateY(-50%);
       max-width: 150%;
       max-height: 100%;
    }
	}

	@media (min-width: 1200px) {
		.image {
		  width: 150px;
      height: 150px;
	    /* background-color: #000; */
	    border-radius: 50%;
	    /* border-color: #fff; */
	    overflow: hidden;
	    position: absolute;
	    /* border-width: 10px 5px 10px 5px; */
	    /* border-style: solid; */
	    border: solid 4px #f0f0f0;
	    border-top-left-radius: 50% 50%;
	    border-top-right-radius: 50% 50%;
	    border-bottom-right-radius: 50% 50%;
	    border-bottom-left-radius: 50% 50%;
		}
    .image .fotimg{
       position: absolute;
       top: 50%;
       left: 50%;
       transform: translateX(-50%) translateY(-50%);
       max-width: 150%;
       max-height: 100%;
    }
	}

      #file-upload-button input{
      	background-color: #f3f3f3;
        border-radius: 25px;
        box-shadow: inset 0px 0px 0px rgba(0,0,0,.2);
        height: 10px; 
      }
      .barra{
        background-color: #f3f3f3;
        border-radius: 25px;
        box-shadow: inset 0px 0px 0px rgba(0,0,0,.2);
        height: 10px; 
      }
      .barra_azul{
        background-color: #247CC0;
        border-radius: 10px;
        display: block;
        height: 25px;
        line-height: center;
        width: 0%;
      }
      .barra_verde{
        background-color: #2EA265 !important;
      }
      .barra_roja{
        background-color: #DE3152 !important;
      }
      #barra_estado span {
        color: #FFF;
        font-weight: bold;
        line-height: 25px;
      }
      .tooltip-inner{
          background: #00a48d !important;
      }
      .bs-tooltip-top .arrow::before {
          border-top-color: #00a48d !important;
      }
</style>
<!-- <script>
var fotoEstado=document.getElementById("fotoEstado").value = '';
console.log(fotoEstado);
</script> -->
<title>UTSEM</title>
  </head>
  <?php require 'user-form_cam_foto.php'; ?>
  


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Carta Pr&oacute;rroga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
                
        <div class="modal-body">
            <div class="alert" hidden role="alert alert-success" id="alert">
                <span id="mens" hidden >Se requiere de una fecha para poder </span><strong id="res" hidden>imprimir la CARTA...</strong>
            </div>
            <p>Una vez oprimiendo el boton de enviar, esta aceptando la entrega de los documentos que hacen falta en la fecha que estableci&oacute;.</p>
            <p>Al mismo tiempo en caso de incumplir, acepto  y tengo de conocimiento que esta es una causa que repercute en mi proceso de selecci&oacute;n y
		    causar baja del proceso de admisi&oacute;n al cual aspiro.</p>
        </div>
    
        <div class="modal-footer">
            <div class="col sm-1">
                <input type="hidden" name="id_solicitante" id="id_solicitante" value="<?php echo SED::encryption($matricula); ?>">
                <input type="date" name="fecha_entrega" id="fecha_entrega" value="" required="" >
            </div>
        <a type="button" class="btn btn-primary" id="des_car_mot" href="./TCPDF/examples/CartaMotivos.php" onClick="return abrir(this.href,this.target)" target="_blank">Enviar </a>
        <script>
        function abrir(url,ubicacion){
            if(document.getElementById("fecha_entrega").value.length === 0) {
                $('#alert').removeAttr("hidden").removeClass("alert-success").addClass("alert-info").fadeIn();
                setTimeout(function(){ $('#alert').removeAttr("hidden").removeClass("alert-success").addClass("alert-info").fadeOut(); }, 4000);
                $('#res').removeAttr("hidden");
                $('#mens').removeAttr("hidden");
        	    return false;
        	}
        	else{
            	 window.open(url+"?fecha_entrega="+document.getElementById('fecha_entrega').value+"&id_solicitante="+document.getElementById('id_solicitante').value, ubicacion, 'width=1000,height=650'); return false;
        	}
        }
        </script>
      </div>
    </div>
  </div>
</div>





  
<!-- Modal2 -->
<div class="modal fade" id="descargarprorroga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Carta Pr&oacute;rroga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
       <div class="modal-body">
        <p>Ya has solicitado anteriormente una prorroga.</p>
      </div>
      
    
      <div class="modal-footer">
        <div class="col sm-1">
            <input type="hidden" name="id_solicitante" id="id_solicitante" value="<?php echo SED::encryption($matricula); ?>">
        </div>
        <a type="button" class="btn btn-primary" id="des_car_mot_1" href="./TCPDF/examples/CartaMotivosImprimir.php" onClick="abrir2(this.href)" target="_blank">Imprimir </a>
        <script>
        function abrir2(url){
            window.open(url+"?id_solicitante=<?php echo SED::encryption($matricula); ?>", 'width=1000,height=650'); return false;
        }
        </script>
      </div>
      
      
    </div>
  </div>
</div>





  <body>
    <div class="container"><br>
    <a type="submit" class="col-md-1 offset-md-10 btn btn-outline-primary btn-block" href="desconectar.php" role="button" style="margin-bottom: 5%;">Salir</a>
      <div class="col-md-12">
        <?php
        require("conexion.php");
          $sql2=$mysqli->query("SELECT * FROM fotos WHERE id_solicitante='$matricula'");
            $count= mysqli_num_rows($sql2);
            if ($count==0) {
              $resultFoto="img/user.png";
              } else {
                $sql=$mysqli->query("SELECT * FROM fotos WHERE id_solicitante='$matricula'");
                $counte= mysqli_num_rows($sql);
                while($mostrar=mysqli_fetch_array($sql)){
                $resultFoto=$mostrar["nombre"];
                
                  if ($counte==0) {
                    $status='';
                  }elseif ($mostrar['status']==0) {
                    $status='';
                  }elseif ($mostrar['status']==1) {
                    $status="<p>Tu foto fue rechazada. VUELVE A TOMARLA... Gracias por tu tiempo";
                  }else{
                    $status="<p>Tu foto fue VALIDADA Gracias por tu tiempo";
                  }
              }
            }
        ?>
       
        <div class="img-responsive image">
          <img src="<?php echo utf8_encode(utf8_decode($resultFoto)); ?>" class="img-responsive fotimg">
        </div>
        
        <div class="btn-group dropright">
        	<button type="button" class="btn-outline-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 50px; border-color:#ffffff00; position: relative; margin: 90px 10px 50px 125px; color: #556080;" required><i class="fa fa-camera centered"></i></button>
          <div class="dropdown-menu">
		    <button class="dropdown-item btn-light" type="button" data-toggle="modal" data-target="#fotos_tomar" style="color: #556080;"><i class="fa fa-camera-retro"></i> Tomar foto...</button>
		  </div>
		</div>
      </div>



  <script type="text/javascript">
    function resul() {
      var resultado = document.getElementsByName("status").value;
      console.log(resultado);
      document.getElementById('lbResultado').innerHTML =resultado;
   }
   </script>
      <div class="col-ms" id="lbResultado" style="color:#00a48d;border-color: #b8daff; text-transform: uppercase;" >
        <input name="status" type="hidden" value="<?= json_encode($status); ?>"
      </div>
      
      <!-- <div class="col-ms" id="status" name="status"></div> -->
      <!-- <div class="col-ms"><?php echo $status; ?></div> -->
      <div class="badge badge-primary text-wrap" style="width: auto; padding: 1em 3em; background-color: #00a48d;">
        MATRICULA: <?php echo $_SESSION['solicitante']['matricula']; ?>
      </div>
      
    <div class="modal-body">
        <p><h5>Foto Obligatorio *</h5>
        Caracteristicas:<br>
        1.- Fondo blanco.<br>
        2.- El cabello largo por atr&aacute;s de los hombros.<br>
        3.- Rostro descubierto (sin fleco o barba).<br>
        4.- Sin gorra, lente, aretes, collares u otros accesorios.<br>
        </p>
    </div>

      <div class="card-header border-0" style="background-color: white; margin-top: 1%;">
        <div class="card-title text-center">
           <img src="img/logo-utsem.png" style="width:90px;" alt="branding logo">
	         <h4 style="text-align: center;">Bienvenid@ <?php echo utf8_encode( $_SESSION['solicitante']['Nombre']);?></h4>
	         <h3 style="text-align: center;">Sistema de Admisión Online </h3>
		    </div>
      </div>

      	<div id="alerta">
            <div class="alert hide" role="alert alert-success" id="alerta">
               <strong id="respuesta"></strong><span id="mensage"></span>
            </div>
        </div>

        <?php
              require("conexion.php");
              $sql1="SELECT * FROM documentos d JOIN usuarios u WHERE d.id_documento not in(SELECT id_documento FROM archivos WHERE id_solicitante = '$matricula' and status!=0) and d.status=1 ORDER BY d.nombre ASC";
              $result1=$mysqli->query($sql1);
              if($mostrar1=mysqli_fetch_array($result1)){
              ?>
        <!-- <form class="form-horizontal" id="form_a_archivo" method="POST" action="a_archivo.php" enctype="multipart/form-data" onsubmit="return ver()"> -->
        
    <form class="form-horizontal" id="form_a_archivo" method="POST" action="a_archivo.php" enctype="multipart/form-data">
        
        <h4 style="text-align: center;">Documentos por entregar</h4>
       	<div class="table-responsive">
       	    
	        <table class="table table-hover">
	            
                <input type="hidden" name="id_solicitante" value="<?php echo $_SESSION['solicitante']['matricula']; ?>">
	            <input type="hidden" name="solicitante" value="<?php echo utf8_encode($_SESSION['solicitante']['Nombre']); ?>">
	            <input type="hidden" name="carrera" value="<?php echo utf8_encode($_SESSION['solicitante']['Carrera']); ?>">
	            <input type="hidden" name="email" value="<?php echo $_SESSION['solicitante']['mail']; ?>">
	            
	            <thead>
	                <tr>
                        <th>No.</th>
	                    <th scope="col">Documento</th>
	                    <th scope="col">obligatorio</th>
	                    <th scope="col">Entregar despues <br>los documentos</th>
	                    <th scope="col">Archivo</th>
	                </tr>
	            </thead>
	            
	            <tbody>
	                <?php require("conexion.php"); ?>
	                <?php $sql="SELECT * FROM documentos d JOIN usuarios u ON d.id_usuario=u.id_usuario 
	                where d.id_documento not in(SELECT id_documento FROM archivos where id_solicitante='$matricula' and status!=0) and d.status=1 ORDER BY d.nombre ASC"; ?>
	                <?php $result=$mysqli->query($sql); ?>
                    <?php $num=1; ?>
	                <?php while($mostrar=mysqli_fetch_array($result)){ ?>
	                
	                <tr>
                        <th scope="row" style="text-transform: uppercase;"><?php echo $num; ?></th>
	                    <th scope="row" style="text-transform: uppercase;"><?php echo utf8_encode(utf8_decode($mostrar['nombre']));?></th>
	                    <input type="text" name="id_documento<?php echo $num; ?>" value="<?php echo $mostrar['id_documento'] ?>" hidden="">
	                    <input type="text" name="documento<?php echo $num; ?>" value="<?php echo $mostrar['nombre'] ?>" hidden="">
	                    
	                    <th scope="row">
	                        <input type="text" style="text-transform: uppercase;" class="form-control" id="miCampo" value="<?php echo $mostrar['obligatorio']?>" readonly>
	                        <input type="hidden" name="document-mandatory-<?php echo $num; ?>" id="document-mandatory-<?php echo $num; ?>" value="<?php echo $mostrar['obligatorio']; ?>"/>
	                    </th>
	                    
	                    <td align="center">
	                        <?php if ($mostrar['obligatorio'] == 'no'){?>
	                            <span class="d-inline-block check-mandatory-<?php echo $num; ?>" tabindex="0">
	                        <?php } ?>
	                        <input type="checkbox" id="prorroga<?php echo $num; ?>" name="prorroga<?php echo $num; ?>" <?php if($mostrar['obligatorio'] == 'si') echo 'disabled="true"'; else echo ''; ?>>
	                    </td>
	                    <td>
	                        <input type="file" class="btn btn-dark" id="archivo[]" accept="image/*,.pdf" name="archivo<?php echo $num; ?>" title="click para buscar tu documento" <?php if($mostrar['obligatorio'] == 'si') echo 'required="true"'; else echo ''; ?> >
	                    </td>
	                    <script type="text/javascript">
                        // 	function datos(e){
                        // 	 var num ='';
                        // 	 var i = '';
                        // 	 var dat = document.getElementById("archivo[]").value[0];
                        // 	 var miC = document.getElementById("miCampo").value;
                        //     // 	 if (dat == null && miC == 'no'){
                        //     // 	     document.getElementById("prorroga[]").required = true;
                        //     // 	 } else {
                        //     // 	     document.getElementById("prorroga[]").required = false;
                        //     // 	 }
                        // 	 console.log(dat);
                        // 	 console.log(num);
                        // 	 console.log(miC);
                        //     }
                        </script>
	                    <!--<td><input style="font-family: monospace; color: #fff; background-color: #343a40; border-color: #343a40" type="file"  class="btn btn-dark" accept="image/*,.pdf" name="archivo[]"></td>-->
	                    <!-- <td><input type="file" class="form-control-file" accept=".pdf,.jpg,.png" id="id_archivo" name="imagen[]"  required=""></td> -->
	                </tr>
	                
	                <?php  $num++; ?>
	                <?php } ?>
	                
	                <input type="text" name="num_documentos" value="<?php echo $num-1; ?>" hidden="">
	            </tbody>
	            
	        </table>
         </div>
        <!-- <div class="barra">
          <div class="barra_azul" id="barra_estado">
            <span></span>
          </div>
        </div> -->
        <!--recaptccha contenerdor de seleccion-->
        <div class="g-recaptcha" data-sitekey="6LcC2c8ZAAAAALjBme4UlFYxuS08IWXr59OPNvXC" data-callback="enabledSubmit"></div>
        <div id="mensageRecaptcha"></div>
        
        <div class="col-md-4 offset-md-4">
        	<input type="submit" class="btn btn-outline-primary btn-block" id="btnSubmit" >
          <!-- <input type="button" class="btn btn-outline-primary btn-block" id="btn_cancelar" value="cancelar"> -->
        </div>
        
    </form>
    <?php } ?>
    
    	<h4 style="text-align: center; margin-top: 7%;">Documentos entregados</h4>
    	<div class="table-responsive">
          <table class="table table-hover">
            
            <thead>
              <tr>
                <th>No.</th>
                <th scope="col">Documento</th>
                 <th scope="col">Prorroga</th>
                <th scope="col" style="text-align: center;">Descargar</th>
                <th scope="col">Archivo</th>
                <th scope="col">Status</th>
                <th scope="col" style="text-align: center;">Comentarios</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              require("conexion.php");
              $sql="SELECT * FROM documentos d 
              JOIN archivos a
              ON d.id_documento=a.id_documento 
              where a.id_solicitante in(SELECT id_solicitante FROM archivos where id_solicitante='$matricula') and d.status=1 and a.status!=0 ORDER BY d.nombre ASC";
              $result=$mysqli->query($sql);
              $nume=1;
              while($mostrar=mysqli_fetch_array($result)){
              	$tipo_archivo=$mostrar['tipo_archivo'];
                $ruta_archivo=$mostrar['ruta_archivo'];
                $nombre_archivo=$mostrar['archivo'];
                
              ?>
              <tr>
                <th scope="row" style="text-transform: uppercase;"><?php echo $nume; $nume++;?></th>

                <th scope="row" style="text-transform: uppercase;"><?php echo $mostrar['nombre']?></th>
                <th scope="row" style="text-transform: uppercase;"><?php echo $mostrar['prorroga']?></th>

                <td align="center">
                    <?php if($mostrar['prorroga']== 'no'){ ?>
                    <a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>" class="text-dark fa fa-download"></a>
                    <?php } ?>
                </td>

                <td><span>Enviado Correctamente</span></td>
                 <!-- <td><input type="file" class="form-control-file" accept=".pdf,.jpg,.png" id="id_archivo" name="imagen[]"  required=""></td> -->
                <?php
                switch ($mostrar['status']) {
                  case '1':
                    $estado="en proceso";
                    break;
                  case '2':
                    $estado="En prorroga";
                    break;
                  case '3':
                    $estado="Rechazado";
                    break;
                    case '4':
                    $estado="Validado";
                    break;
                    
                  default:
                    $estado="Lo sentimos este archivo fue desabilitado por el administrador..";
                    break;
                }
                 ?>
                <td><?php echo  $estado?></td>
                <td style="text-align: center;"><?php echo $mostrar['comentario']?>
                </td>
              
              <td>
              <form id="e_archivos_solicitante" method="POST" action="e_archivos_solicitante.php">
                <input type="hidden" name="id_archivo" value="<?php echo $mostrar['id_archivo'] ?>">
                <button type="submit" name="id_archivo" id="archivoEli" class="btn btn-outline-dark">
                <i class="fa fa-trash-alt centered"></i>
              </form>
              </td>
            </tr>
                 <?php
                }
                ?>
            </tbody>
          </table>

            <!--<p>Si no cuentas con todos los documentos descargar: <a type="submit" id="des_car_mot" href="./TCPDF/examples/CartaMotivos.php?id_solicitante=<?php echo SED::encryption($_SESSION['solicitante']['matricula']) ?>" target="_blank"> Carta Motivo</a> </p>-->
             <!--<p>Importante! <br>Si seleccionaste las casillas generar: <a href="" data-toggle="modal" data-target="#exampleModal">carta pr&oacute;rroga</a> </p>-->
             <!--Programo el evento onclick con una funci��n llamada checkProrroga que se encargar�� de evaluar si un alumno ya solicit�� prorroga o no-->
             
             <?php if(mysqli_num_rows($result)>0){
                    $sql=$mysqli->query("SELECT * FROM archivos where id_solicitante='$matricula'");
                    while($mostrar=mysqli_fetch_array($sql)){
                    if($mostrar['prorroga'] == 'si'){ ?>
             <p>Importante! <br>Si seleccionaste las casillas generar: <a href="#" onclick="checkProrroga();">carta pr&oacute;rroga</a> </p>
             <?php } } }?>
             
             
      	</div>
    </div>

<?php

    $id_solicitante = $_SESSION['solicitante']['matricula'];
    $solicitante = $_SESSION['solicitante']['Nombre'];
    $carrera = $_SESSION['solicitante']['Carrera'];
    $email = $_SESSION['solicitante']['mail'];
    $curp = $_SESSION['solicitante']['CURP'];
    date_default_timezone_set('America/Mexico_city');
	$date_create=date("y-m-d H:i:s");
	$date_update=date("y-m-d H:i:s");
	$status=0;
	
    $bus=$mysqli->query("SELECT * FROM solicitantes WHERE id_solicitante='$id_solicitante' ");
	$count= mysqli_num_rows($bus);

	if ($count != 0) {
		$upd=$mysqli->query("UPDATE solicitantes SET date_update='$date_update', email='$email', curp='$curp' WHERE id_solicitante='$id_solicitante'");
	} else {
		$ins=$mysqli->query("INSERT INTO solicitantes VALUES ('$id_solicitante','$solicitante','$carrera','$email','$curp','$status','$date_create', '')");
	
	    //echo 'se inserto';
	}
	
	
    
?>
	
	<!--Recaptcha Script-->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <!-- <script src="r_u.js"></script> -->
    <!-- <script src="sc_fotos.js"></script> -->
<script type="text/javascript">
/*Creo la funci��n checkProrroga*/
function checkProrroga(){
    <?php 
        $consulta_prorroga = $mysqli->query("SELECT * FROM prorroga WHERE status=0 AND id_solicitante='$matricula'");
        if(mysqli_num_rows($consulta_prorroga) > 0){
            $sql = $mysqli->query("SELECT p.status, p.date_create, p.date_update, p.fecha_entrega, p.id_solicitante FROM archivos a JOIN prorroga p ON a.id_solicitante = p.id_solicitante JOIN documentos d ON a.id_documento = d.id_documento WHERE a.id_solicitante = '$matricula' AND a.prorroga = 'SI'");
             while ($row=mysqli_fetch_array($sql)){
                // son las formas de sacar la fecha actual
                $fecha_actual = new DateTime(date('Y-m-d'));
                $fechaHoy = date('Y-m-d H:i:s');
                //ala fecha de creacion le suma 30 de la prorroga
                $date = $row['date_create'];
                $da = strtotime($date."+ 30 days");
                $cadatreintadias = date('Y-m-d H:i:s', $da);
                // ala fecha de actualizacion se le suma 30 dias de la prorroga
                $date = $row['date_update'];
                $da = strtotime($date."+ 30 days");
                $cadatreintadiasUpdate = date('Y-m-d H:i:s', $da);
                // hace la resta de la fecha a cumplir o entregar
                $fecha_final = new DateTime($row['fecha_entrega']);
                $diasSolicitud = $fecha_actual->diff($fecha_final)->format('%r%a');
                // si los 30 dias a la fecha de la creacion es mayor entonces se cumple con condicion y puedo descargar prorroga
                if ($cadatreintadias > $fechaHoy){ ?>
                    /*Si la consulta arroja como m��nimo un dato mando una alerta*/
                    /*alert("Ya has solicitado anteriormente una prorroga");*/
                    $("#descargarprorroga").modal("show");
                    // si los 30 dias que se le sumaron al fecha de actuliacion es mayor a la fecha actual cumple la condicion y puedo descargar prorroga
                <?php }elseif($cadatreintadiasUpdate > $fechaHoy){ ?>
                    /*Si la consulta arroja como m��nimo un dato mando una alerta*/
                    /*alert("Ya has solicitado anteriormente una prorroga");*/
                    $("#descargarprorroga").modal("show");
                    // si la fecha de entrega es mayor a 0 entonces cumple la condicion y puedo descargar prorroga
                <?php }elseif($diasSolicitud > 0){ ?>
                    /*Si la consulta arroja como m��nimo un dato mando una alerta*/
                    /*alert("Ya has solicitado anteriormente una prorroga");*/
                    $("#descargarprorroga").modal("show");
                <?php }else{ ?>
                    //si no se cumplen ninguna de las condiciones entonces puedes pedir la prorroga 
                    /*Si no hay registros de ese alumno lanzo a pantalla el modar*/
                    $("#exampleModal").modal("show");
                <?php }
            }
         }else{ ?>
             $("#exampleModal").modal("show");
        <?php  } ?>
}

    // function ver() {
    //     var miCampoArchivo = document.getElementById("miCampo").value;
    //     // console.log(miCampoArchivo);
    //     if (miCampoArchivo.length == si(miCampoArchivo)) {
    //         alert('Documento Obligatorio!');
    //     return false;
    //     }
    // }

// function verific(id_documento,fileVerific) {
// 	if(document.getElementById("id_documento").value[0].length === 0) {
// 	    $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-warning").fadeIn();
//         setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-warning").fadeOut(); }, 1500);
// 		$("#mensage").html("NO ha SUBIDO ningun documento, se requiere que suba documento antes de dar CLIC enviar...");
		
// 	}else if(document.getElementById("fileVerific").value.length === 0) {
// 	    $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-warning").fadeIn();
//         setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-warning").fadeOut(); }, 1500);
// 		$("#mensage").html("NO ha SUBIDO ningun documento, se requiere que suba documento antes de dar CLIC enviar...");
// 		return false;
// 	}
// };

$(document).on('submit', '#e_archivos_solicitante', function(){
  event.preventDefault();
  $.ajax({
    url: $(this).attr('action'),
    type: $(this).attr('method'),
    dataType: 'json',
    data: $(this).serialize(),
    beforeSend: function(){
    //   $('#archivoEli').attr('disabled', 'true');
    }
  })

  .done(function(respuesta){
    if (!respuesta.error) {
      if (respuesta.tipo == 1 || respuesta.tipo == 2) {
        // $('#archivoEli').removeAttr('disabled');
        $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
        setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 1000);
        $("#respuesta").html("Hecho!");
        $("#mensage").html(" se elimino correctamente...");
        document.location.href='user-form.php';
      }else if (respuesta.tipo == 4) {
        // $('#archivoEli').removeAttr('disabled');
        $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
        setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 2000);
        $("#respuesta").html("error! ");
        $("#mensage").html("NO ha se apodido eliminar, por que ya fue validado...");
      }

    }else{
    	$('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
    		 setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 2000);
        $("#respuesta").html("OCURRIO UN ERROR! ");
        $("#mensage").html("NO ha se apodido eliminar, los datos no llegan al servidor...");
        document.location.href='user-form.php'; 
    }
  })
  .fail(function(respuesta){
  })
  .done(function(){
  });
});


    $(document).ready(function() {
       $("#form_a_archivo").on("submit", function() {
          event.preventDefault();
          var check = false;
          var formData = new FormData(this);
          
          var response = grecaptcha.getResponse();
          var recaptcha = false;
          
        for(var x = 1; x <= <?php echo ($num - 1)?>; x++){
            if($('#document-mandatory-'+x).val() == 'no'){
                if($("#prorroga"+x).prop("checked") == false && $("[name='archivo"+x+"']").get(0).files.length == 0){
                    check = true;
                    $(".check-mandatory-"+x).attr("title","Debe seleccionar esta casilla o subir el archivo").attr("data-original-title", "Debe seleccionar esta casilla o subir el archivo");
                    $('.check-mandatory-'+x).tooltip('show');
                    $('#btnSubmit').val('Seleccionar casilla').removeClass('btn btn-outline-primary btn-block').addClass('btn btn-outline-danger btn-block');
                }else{
                    $(".check-mandatory-"+x).attr("data-original-title", "");
                    $(".check-mandatory-"+x).attr("title", "");
                    $('.check-mandatory-'+x).tooltip('hide');
                }
            }
        }
         
        if(response.length == 0){
            $('#mensageRecaptcha').html('<p class="text-danger center">Captcha no verificado y es obligatorio. *</p>');
            recaptcha = true;
            $('#btnSubmit').val('Capturar recaptcha').removeClass('btn btn-outline-primary btn-block').addClass('btn btn-outline-danger btn-block');
        } else {
            $('#mensageRecaptcha').html('<p class="text center">Esta pregunta es para comprobar que seas un visitante humano y prevenir envios de spam automatizado.</p>');

        /*for(var i of formData.entries()){
            console.log(i[0]+" - "+i[1]);
        }*/

            if(check == false && recaptcha == false){
                $.ajax({
                  url: $(this).attr('action'),        
                  type: $(this).attr('method'),
                  dataType: 'json',
                  data: formData,
                  contentType: false,
                  processData: false,
                  cache: false,
                  beforeSend: function(){
                    $('#btnSubmit').val('subiendo...').removeClass('btn btn-outline-danger btn-block').addClass('btn btn-outline-primary btn-block');
                    $('#btnSubmit').attr("disabled", "disabled");
                  },
                  success: function(respuesta) {
                  if(respuesta.error == false) {
                    $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
                    $("#mensage").html(respuesta.mensaje);
                    $('#btnSubmit').val('Enviar');
                    $("#form_a_archivo").fadeOut();
                    setTimeout(function(){ 
                        $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut();
                        location.href='user-form.php';
                        window.location.reload()
                    }, 10000);
                    
                  } else if(respuesta.error == true){
                    $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
                    setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 5000);
                    $("#mensage").html(respuesta.mensaje);
                    $('#btnSubmit').val('reenviar');
                  }
                  },
                  error: function(req, err){
                     console.log(req);
                     console.log(err);
                    $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
                    $("#respuesta").html("error! ");
                    $("#mensage").html("NO ha sido posible comunicarse con el servidor...");
                  }
                });
            }
            }
            return false;
          });
        });


    $(document).ready(function() {
       $("#form_foto_webcam").on("submit", function() {
          event.preventDefault();

            $.ajax({
              url: 'a_fotos.php',        
              type: $(this).attr('method'),
              dataType: 'json',
              data: $(this).serialize(),
              cache: false,
              beforeSend: function(){
                $('#subirFoto').val('cargando...');
              },
              success: function(respuesta) {
              if(respuesta.error.error == false) {
                $('#estado').html(respuesta.error.mensaje);
                location.href='user-form.php';
              } else if(respuesta.error.error == true){
                $('#estado').html(respuesta.error.mensaje);
                $('#subirFoto').val('reenviar');
              }
              },
              error: function(respuesta){
                $("#estado").html(respuesta.error.mensaje);
                $('#subirFoto').val('reenviar');
              }
            });
            return false;
          });
        });
        
    // $(document).on("submit", '#des_car_mot' function() {
    // //   console.log()"descargar";
    // wimdow.open("./pdfs/index.php?id_solicitante=<?php echo $_SESSION['solicitante']['matricula']; ?>", "_blank");
    // });
    
</script>
</body>
</html>