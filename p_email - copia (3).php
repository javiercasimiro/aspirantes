<?php
require_once 'vendor/autoload.php';
require('conexion.php');

    if (isset($_POST["id_archivo"]) && isset($_POST["id_documento"]) && isset($_POST["id_solicitante"]) && 
      isset($_POST["solicitante"]) && isset($_POST["email"]) && isset($_POST["archivo"]) && 
      isset($_POST["ruta_archivo"]) && isset($_POST["tipo_archivo"]) && isset($_POST["comentario"]) && 
      isset($_POST["status"]) && isset($_POST["date_create"])) {

      foreach ($_POST['id_archivo'] as $key => $id_archivo) {
          $id_archivo=$mysqli->real_escape_string($_POST["id_archivo"][$key]);
          $id_documento=$mysqli->real_escape_string($_POST["id_documento"][$key]);
          $id_solicitante=$mysqli->real_escape_string($_POST["id_solicitante"][$key]);
          $solicitante=$mysqli->real_escape_string($_POST["solicitante"][$key]);
          $email=$mysqli->real_escape_string($_POST["email"][$key]);
          $archivo=$mysqli->real_escape_string($_POST["archivo"][$key]);
          $ruta_archivo=$mysqli->real_escape_string($_POST["ruta_archivo"][$key]);
          $tipo_archivo=$mysqli->real_escape_string($_POST["tipo_archivo"][$key]);
          $comentario=$mysqli->real_escape_string($_POST["comentario"][$key]);
          $status=$mysqli->real_escape_string($_POST["status"][$key]);
          $statuss=$mysqli->real_escape_string($_POST["status"][$key]);
          $nombre=$mysqli->real_escape_string($_POST["nombre"][$key]);
          $date_create=$mysqli->real_escape_string($_POST["date_create"][$key]);
          date_default_timezone_set('America/Mexico_city');
          $date_update=date("y-m-d H:i:s");

          $res=$mysqli->query("UPDATE archivos SET id_archivo = '$id_archivo', id_documento ='$id_documento', id_solicitante ='$id_solicitante', solicitante ='$solicitante', email ='$email', archivo ='$archivo', ruta_archivo ='$ruta_archivo', tipo_archivo ='$tipo_archivo', comentario ='$comentario', status ='$status', date_create='$date_create', date_update='$date_update' where id_archivo = '$id_archivo'");
      }
              if ($statuss ==1) {
                $statuss='se encuentra en proceso, espere noticias';
              }elseif ($statuss == 2) {
                $statuss='fue rechazado por no cumplir con los requisitos solicitados';
              }elseif ($statuss == 3){
                $statuss='fue validado, Gracias por cumplir con los requisitos solicitados';
              }
            //Administrador
            $administradorEmail='chirinos.jesus1234@gmail.com';
            $body = '
              <html lang="en">
                <head><meta charset="gb18030">
                  
                  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                </head>
                <body style="font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji">
                    <h5 style="margin-left: 10%; margin-top: 4%; font-size: 1.25rem;">100% BUHOS...</h5><br>
                    <div align="center">
                      <img src="https://i.postimg.cc/VvVJwQjY/logo-utsem-png.png" style="width:100px;" alt="branding logo">
                    </div> 
                    <div align="center" style="margin-top: 2%; margin-bottom: 5%;">
                      <h3>Hola! <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$solicitante.'</small></h3>
                    </div>
                      <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">Hemos reviso tu documentación y el documento llamado: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller; text-transform: uppercase;">'.$nombre.'</small></h5>
                      <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">El cual: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$statuss.'</small></h5>
                      <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">Comentario: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$comentario.'</small></h5>
                      <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">Fecha de verificación: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$date_create.'</small></h5>
                    <div align="center">
                      <a href="http://admisionut.utsem-morelos.edu.mx/" style="background-color: #00a48d; border-color: #00a48d; color: #FFF; margin-top: 10%; display: inline-block; font-weight: 400; text-align: center; vertical-align: middle; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; border: 1px solid transparent; padding: .375rem .75rem; font-size: 1rem; line-height: 1.5; border-radius: .25rem; transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;">Conocenos</a> 
                    </div>   
                </body>
              </html>';

              $transport = (new Swift_SmtpTransport('mail.utsem-morelos.edu.mx', 25))
                ->setUsername('javiercasimiro@utsem-morelos.edu.mx')
                ->setPassword('utsemjavier')
              ;

              // Create the Mailer using your created Transport
              $mailer = new Swift_Mailer($transport);

              // Create a message
              $message = (new Swift_Message('Universidad Tecnológica del Sur del Estado de Morelos UTSEM'))
                ->setFrom(['javiercasimiro@utsem-morelos.edu.mx' => 'UTSEM'])
                ->setTo([$administradorEmail => $solicitante])
                ->setBody($body)
                ;

              // Send the message
              $result = $mailer->send($message);

      
      $return['error'] = array('error' => false);
    } else{
      $return['error'] = array('error' => true);
    }
  
echo json_encode($return);
//$mysqli->close();
?>