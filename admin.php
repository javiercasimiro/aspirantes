<?php
session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
?>
<html lang="en">
  <head><meta charset="gb18030">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>
  <body>
    <div class="container"><br>
      <a type="submit" class="col-md-1 offset-md-10 btn btn-outline-primary btn-block" href="desconectar.php" role="button" style="margin-bottom: 5%;">Salir</a>
      <div class="badge badge-primary text-wrap" style="width: auto; padding: 1em 2em; background-color: #00a48d; margin-bottom: 5%;">
        <?php echo $_SESSION['correo_usu']?>  
      </div>
    
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo">        
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
            <span>Bienvenid@ - <?php echo $_SESSION['nombre_usu']?></span><br>
            <span>Menu</span>
          </h6>
      </div>         
    </div>
    <!-- BEGIN: Content-->
    <div class="row">
      <div class="col-sm-4">
        <div class="card">
          <div class="card-body">
              <a href="re_documentos.php" class="card-link"> <img src="img/documentos.png" style="width:20%; margin-right: 5%;" class="card-img-center" alt="...">Control de Documentos</a>
          </div>
        </div>
      </div>


      <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
              <a href="re_administrador.php" class="card-link"> <img src="img/administrativo.png" style="width:20%; margin-right: 5%;" class="card-img-center" alt="...">Gestión de Usuarios</a>
          </div>
        </div>
      </div>


      <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
              <a href="re_archivos.php" class="card-link"> <img src="img/archivos.png" style="width:20%; margin-right: 5%;" class="card-img-center" alt="...">Revisión Archivos</a>
          </div>
        </div>
      </div>

<br>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
              <a href="periodo/re_periodo.php" class="card-link"> <img src="img/icon_configuracion.png" style="width:20%; margin-right: 5%;" class="card-img-center" alt="...">Configuración</a>
          </div>
        </div>
      </div>

<br>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
              <a href="reportes.php" class="card-link"> <img src="img/icon_reporte.png" style="width:20%; margin-right: 5%;" class="card-img-center" alt="...">Reportes</a>
          </div>
        </div>
      </div>
      
      

    </div>
</div>

<!-- END: Content-->

      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript">
// var timeout;
// var base_url = 'fake_url';
// document.onmousemove = function() {
//   clearTimeout(timeout);
//   timeout = setTimeout(function() {
//     $.confirm({
//       title: 'Cierre de Sesion Automatica',
//       content: 'Si deseas salir haz clic en Cerrar Sesion o en Cancel para continuar trabajando',
//       buttons: {
//         confirm: function() {
//           $.ajax({
//             url: base_url + "auth/logout",
//             type: "POST",
//             success: function(resp) {
//               window.location.href = base_url;
//             }
//           });
//         },
//         cancel: function() {
//           window.location.href = base_url + "movimientos/ordenes";
//         },
//       }
//     });
//   }, 3000);
// }

  var timeout; 
    document.onmousemove = function(){ 
        clearTimeout(timeout); 
        timeout = setTimeout(function(){
            $.confirm({
              title: "Tu sesión ha expirado",
              text: '',
              type: "warning",
              showCancelButton: true,
              cancelButtonText: "Continuar Trabajando",
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Salir",
              closeOnConfirm: false,
              showLoaderOnConfirm: true,
            },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: base_url + "index.php",
                        type: "POST",
                        success:function(resp){
                            window.location.href= base_url;
                        }

                    });
                }else{
                    location.reload();
                }

            });
        }, 1000); 
    }
    </script>
  </body>
</html>