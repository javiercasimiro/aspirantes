<?php
session_start();
if ($_SESSION['solicitante'] == '') {
  header('Location: l-usuario.php');

}
$id_usuario=1;
$matricula =$_SESSION['solicitante']['matricula'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>
  <body>
    <div class="container"><br>
      <a type="submit" class="col-md-1 offset-md-10 btn btn-outline-primary btn-block" href="desconectar.php" role="button" style="margin-bottom: 5%;">Salir</a>
      <div class="badge badge-primary text-wrap" style="width: auto; padding: 1em 2em; background-color: #00a48d;">
        MATRICULA: <?php echo $_SESSION['solicitante']['matricula']; ?>
      </div>
      
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo">
        </div>
      </div>
      <h4 style="text-align: center;">Bienvenid@ <?php echo $_SESSION['solicitante']['Nombre'];?></h4>
      <h3 style="text-align: center;">Sistema de Admisión Online </h3>

      	<div id="alerta">
            <div class="alert hide" role="alert alert-success" id="alerta">
               <strong id="respuesta"></strong><span id="mensage"></span>
            </div>
        </div>

        <?php
              require("conexion.php");
              // $sql1="SELECT * FROM documentos d JOIN archivos a
              // ON d.id_documento=a.id_documento 
              // JOIN usuarios u
              // ON u.id_usuario=d.id_usuario
              // where d.id_documento not in(SELECT id_documento FROM archivos where id_solicitante='$matricula')";
              $sql1="SELECT * FROM documentos d JOIN usuarios u WHERE d.id_documento not in(SELECT id_documento FROM archivos WHERE id_solicitante = '$matricula' and status!=0) and d.status=1";
              $result1=$mysqli->query($sql1);
              if($mostrar1=mysqli_fetch_array($result1)){
              ?>
        <h4 style="text-align: center;">Documentos por entregar</h4>
        <form class="form-horizontal" id="form_a_archivo" method="POST" action="a_archivo.php" enctype="multipart/form-data">
       	  <div class="table-responsive">
	        <table class="table table-hover">

	            <thead>
	              <tr>
	                <th scope="col">Documento</th>
	                <th scope="col">Archivo</th>
	              </tr>
	            </thead>
	            <tbody>
	              <?php
	              require("conexion.php");
	              $sql="SELECT * FROM documentos d 
	              JOIN usuarios u
	              ON d.id_usuario=u.id_usuario 
	              where d.id_documento not in(SELECT id_documento FROM archivos where id_solicitante='$matricula' and status!=0) and d.status=1";
	              $result=$mysqli->query($sql);
	              while($mostrar=mysqli_fetch_array($result)){
	              ?>
	              <tr>
	                <th scope="row" style="text-transform: uppercase;"><?php echo $mostrar['nombre']?></th>

	                <td><input type="hidden" name="id_solicitante" value="<?php echo $_SESSION['solicitante']['matricula']; ?>">
	                <input type="hidden" name="solicitante" value="<?php echo $_SESSION['solicitante']['Nombre']; ?>">
	                <input type="hidden" name="email" value="<?php echo $_SESSION['solicitante']['mail']; ?>">
	                <input type="text" name="id_documento[]" value="<?php echo $mostrar['id_documento'] ?>" hidden="">
	                <input type="file" class="form-control-file" accept="image/*,.pdf" name="archivo[]" required=""></td>
	                 <!-- <td><input type="file" class="form-control-file" accept=".pdf,.jpg,.png" id="id_archivo" name="imagen[]"  required=""></td> -->
	              </tr>
	                 <?php
	                }
	                ?>
	            </tbody>
	        </table>
         </div>
        <div class="col-md-2 offset-md-5">
        	<button type="submit" class="btn btn-outline-primary btn-block" id="btnSubmit">Enviar</button>
        </div>
    </form>
    <?php } ?>
    
    	<h4 style="text-align: center; margin-top: 7%;">Documentos entregados</h4>
    	<div class="table-responsive">
          <table class="table table-hover">
            
            <thead>
              <tr>
                <th scope="col">Documento</th>
                <th scope="col">Descargar</th>
                <th scope="col">Archivo</th>
                <th scope="col">Status</th>
                <th scope="col" style="text-align: center;">Comentarios</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              require("conexion.php");
              $sql="SELECT * FROM documentos d 
              JOIN archivos a
              ON d.id_documento=a.id_documento 
              where a.id_solicitante in(SELECT id_solicitante FROM archivos where id_solicitante='$matricula') and d.status=1 and a.status!=0";

              $result=$mysqli->query($sql);
              while($mostrar=mysqli_fetch_array($result)){
              	$tipo_archivo=$mostrar['tipo_archivo'];
                $ruta_archivo=$mostrar['ruta_archivo'];
                $nombre_archivo=$mostrar['archivo'];
              ?>
              <tr>
                <th scope="row" style="text-transform: uppercase;"><?php echo $mostrar['nombre']?></th>

                <td align="center"><a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>" class="text-dark fa fa-download"></a></td>

                <td><span>Recibido</span></td>
                 <!-- <td><input type="file" class="form-control-file" accept=".pdf,.jpg,.png" id="id_archivo" name="imagen[]"  required=""></td> -->
                <?php
                switch ($mostrar['status']) {
                  case '1':
                    $estado="en proceso";
                    break;
                  case '2':
                    $estado="Validado";
                    break;
                  case '3':
                    $estado="Rechazado";
                    break;
                  default:
                    $estado="en proceso";
                    break;
                }
                 ?>
                <td><?php echo  $estado?></td>
                <td style="text-align: center;"><?php echo $mostrar['comentario']?>
                </td>
              
              <td>
              <form id="e_archivos_solicitante" method="POST" action="e_archivos_solicitante.php">
                <input type="hidden" name="id_archivo" value="<?php echo $mostrar['id_archivo'] ?>">
                <button type="submit" name="id_archivo" id="archivoEli">
                <i class="fa fa-trash-alt centered"></i>
              </form>
              </td>
            </tr>
                 <?php
                }
                ?>
            </tbody>
          </table>
      	</div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).on('submit', '#e_archivos_solicitante', function(event){
  event.preventDefault();

  $.ajax({
    url: $(this).attr('action'),
    type: $(this).attr('method'),
    dataType: 'json',
    data: $(this).serialize(),
    beforeSend: function(){
    }
  })
  .done(function(respuesta){
    if (!respuesta.error) {
      if (respuesta.tipo == 1 || respuesta.tipo == 3) {
      	//$('#archivoEli').attr('disabled', 'true').removeClass("disabled");
        $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
    		setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 1000);
    	$("#respuesta").html("Hecho!");
        $("#mensage").html(" se elimino correctamente...");
        document.location.href='user-form.php';
      }else if (respuesta.tipo == 2) {
		$('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
    		setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 2000);
    	$("#respuesta").html("error! ");
        $("#mensage").html("NO ha se apodido eliminar, por que ya fue validado...");
      }
    }else{
    	$('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
    		setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 2000);
    	$("#respuesta").html("OCURRIO UN ERROR! ");
        $("#mensage").html("NO ha se apodido eliminar, los datos no llegan al servidor...");
        location.href='user-form.php';
    }
  })
  .fail(function(respuesta){
  })
  .done(function(){
  // if (($(this).val() != 2)) {
  //   $('#archivoEli').prop('disabled', false);
  //   return true;
  // } else {
  //   $('#archivoEli').prop('disabled', true);
  //   return false;
  // }
 //  if ($('.id_archivo').value == 2 ) {
 // 	$('#archivoEli').prop('disabled', true);
	//   $('#archivoEli').keyup(function() {
	//    if($(this).val() != 2) {
	//      $('#archivoEli').prop('disabled', false)
	//   }
	// })
	// }
    // console.log('');
  });
});


    $(document).ready(function() {
       $("#form_a_archivo").bind("submit", function(e) {
          e.preventDefault();
            $.ajax({
              url: $(this).attr('action'),        
              type: $(this).attr('method'),
              dataType: 'json',
              data: $(this).serialize(),
              beforeSend: function(){
                
              },
              success: function(respuesta) {
              if(respuesta.error == false) {
                $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
                   setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 1500);
                $("#mensage").html(respuesta.error.mensage);
              }else if(respuesta.error == true){
                $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
                   setTimeout(function(){ $("alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 1500);
                $("#mensage").html(respuesta.error.mensage);
              }
              },
              error: function(respuesta){
                $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
                $("#respuesta").html("error! ");
                $("#mensage").html("el mensage NO ha sido enviado");
              }
            });
            return false;
          });
        });

// $(document).on('submit', '#a_archivo', function(event){
//   event.preventDefault();
  
//   $.ajax({
//     url: $(this).attr('action'),
//     type: $(this).attr('method'),
//     dataType: 'json',
//     enctype: $(this).attr('multipart/form-data'),
//     data: $(this).serialize(),
//     beforeSend: function(){
//       //$('#btnSubmit').attr('disabled', 'true');
//     },
//     success: function(respuesta) {
//       if (respuesta.error == null) {
//         //$('#btnSubmit').removeAttr('disabled');
//         $('#mensage').html(respuesta.error.men);
//         $('#alerta').fadeIn();
//            setTimeout(function(){ $('#alerta').fadeOut(); }, 2500);
//       }else if(respuesta.error == true){
//         document.location.href = 'user-form.php';
//       }
//     },
//     error: function(respuesta){
//     $('#btnSubmit').removeAttr('disabled');   
//     }
//     })
// });


// $( document ).ready(function() {
// $.confirm({
//     title: '¡Alerta!',
//     content: 'Ha sobrepasado el tiempo permitido',
//     autoClose: 'logoutUser',
//     buttons: {
//       logoutUser: {
//         text: 'Iniciar sesion',
//         action: function () {
//           $.alert('La sesión ha expirado');
//           window.location.replace('index.php')
//         }
//       },
//     }
// });
// });

// $( document ).ready(function() {
//     $.confirm({
//        title: 'Alerta!',
//         content: 'La sesión esta por expirar.',
//         autoClose: 'logoutUser|60',
//         buttons: {
//             logoutUser: {
//                 text: 'cerrar sesión',
//                 action: function () {
//                     $.alert('La sesión ha expirado');
//                     //tu codigo AJAX                    
//                 }
//             },
//             cancelar: function () {
//                 $.alert('cancelado');
//             }
//         }
//     });
// });

// function salir(e){
//   <?php 

//     $_SESSION['solicitante'] = '';
//    ?>
//    document.location.href = 'index.php';
// };

  // var timeout; 
  //   document.onmousemove = function(){ 
  //       clearTimeout(timeout); 
  //       timeout = setTimeout(function(){
  //           swal({
  //             title: "Tu sesión ha expirado",
  //             text: '',
  //             type: "warning",
  //             showCancelButton: true,
  //             cancelButtonText: "Continuar Trabajando",
  //             confirmButtonClass: "btn-danger",
  //             confirmButtonText: "Salir",
  //             closeOnConfirm: false,
  //             showLoaderOnConfirm: true,
  //           },
  //           function(isConfirm){
  //               if (isConfirm) {
  //                   $.ajax({
  //                       url: base_url + "index.php",
  //                       type: "POST",
  //                       success:function(resp){
  //                           window.location.href= base_url;
  //                       }

  //                   });
  //               }else{
  //                   location.reload();
  //               }

  //           });
  //       }, 60); 
  //   } 

    // function liberar(){
    // <?php
    // if(isset($_POST['id_documento']) || $_POST['id_documento'] === ''){
    // $id_documento = $_POST['id_documento'];

    //   $result = $mysqli->query("SELECT * FROM documentos WHERE id_documento='$id_documento' AND status=1");
    //   $count = mysqli_num_rows($result);
    //   if($count!=0) {
    //     header('location:re_documentos.php');
    //     //return false;
    //   }else{
    //     $mysqli->query("UPDATE documentos SET status=1 WHERE id_documento='$id_documento'");
    //     header("location:re_documentos.php");
    //     //return true;
    //     }
    //   }
    //     ?>
    //   }

</script>

  </body>
</html>