<?php
session_start();
require("conexion.php");
require("e_d.php");
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
if(isset($_GET['id_solicitante'])) $id_solicitante = SED::descryption($_GET['id_solicitante']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>
<body>
  <div class="container"><br>
      <a class="btn btn-primary offset-md-10" href="re_archivos.php" role="button" style="margin-bottom: 5%;">regresar</a>
      <div class="badge badge-primary text-wrap" style="width: auto; padding: 1em 2em; background-color: #00a48d;">
        MONBRE: <?php $session_sol="SELECT 
                  a.id_archivo,
                  a.id_documento,
                  a.id_solicitante,
                  a.solicitante,
                  a.email,
                  a.archivo,
                  a.ruta_archivo,
                  a.tipo_archivo,
                  a.comentario,
                  a.status,
                  a.date_create,
                  d.nombre,
                  u.id_usuario
                FROM archivos a JOIN documentos d
                ON a.id_documento=d.id_documento
                JOIN usuarios u
                ON d.id_usuario=u.id_usuario 
                where a.id_solicitante = '$id_solicitante';
                
                 ";
              $ressesion=$mysqli->query($session_sol);
              if($sol=mysqli_fetch_array($ressesion)){

                echo $solicitante=$sol[3];
                $id_solicitate=$sol[2];
              } ?><br><br>MATRICULA: <?php echo $id_solicitate ?>
      </div>
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo">
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          <h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>confirma, comenta y guarda</span>
          </h6>
        </div>
      </div>
      <?php
      $documentos="SELECT distinct
                  a.id_archivo,
                  a.id_documento,
                  a.id_solicitante,
                  a.solicitante,
                  a.email,
                  a.archivo,
                  a.ruta_archivo,
                  a.tipo_archivo,
                  a.comentario,
                  a.status,
                  a.date_create,
                  d.nombre,
                  u.id_usuario
                FROM archivos a JOIN documentos d
                ON a.id_documento=d.id_documento
                JOIN usuarios u
                ON d.id_usuario=u.id_usuario 
                where a.id_solicitante = '$id_solicitante'
                and a.status!=0 and d.status!=0
                group by  a.id_archivo, a.id_documento, a.id_solicitante
                ORDER BY d.nombre ASC";
              $resultdocumentos=$mysqli->query($documentos);
              if($mostrar=mysqli_fetch_array($resultdocumentos)) {
       ?>

        <!-- <form id="formArch" class="form-horizontal"  method="POST" action="p_email.php">  -->
          
        <div id="alerta">
            <div class="alert hide" role="alert alert-success" id="alerta">
               <strong id="respuesta"></strong><span id="mensage"></span>
            </div>
        </div>
<!--         <div id="loginInfo" >
        <button type="button" class="close" data-dismiss="alert hide" aria-label="Close" aria-hidden="true">&times;</button>
        <strong id="respuesta"></strong id="mensage"><span></span>
        </div>
 -->       
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Documento</th>
                <th scope="col" style="text-align: center;">Descargar</th>
                <th scope="col" style="text-align: center;">Status</th>
                <th scope="col" style="text-align: center;">Comentarios</th>
                <th scope="col" style="text-align: center;">Guardar</th>  
              </tr>
            </thead>
            <tbody>
            <?php
               $sql="SELECT distinct
                  a.id_archivo,
                  a.id_documento,
                  a.id_solicitante,
                  a.solicitante,
                  a.email,
                  a.archivo,
                  a.ruta_archivo,
                  a.tipo_archivo,
                  a.comentario,
                  a.status,
                  a.date_create,
                  d.nombre,
                  u.id_usuario
                FROM archivos a JOIN documentos d
                ON a.id_documento=d.id_documento
                JOIN usuarios u
                ON d.id_usuario=u.id_usuario 
                where a.id_solicitante = '$id_solicitante'
                and a.status!=0 and d.status!=0
                group by  a.id_archivo, a.id_documento, a.id_solicitante
                ORDER BY d.nombre ASC";
              $tipo_archivo="";
              $archivo="No encontrado";
              $ruta_archivo="";
              $query_db=$mysqli->query($sql);
              while($row=mysqli_fetch_array($query_db)){
                  //$archivo="Descargar";
                  $tipo_archivo=$row[7];
                  $ruta_archivo=$row[6];
                  $nombre_archivo=$row[5];
                switch ($row[9]) {
                  case '1':
                    $status="En proceso...";
                    break;
                  case '2':
                    $status="Rechazado";
                    break;
                  case '3':
                    $status="Validado";
                    break;
                  default:
                    $status="Lo sentimos este archivo fue desabilitado por el administrador...";
                    break;
                  }
               ?>
            <tr>
             <!--  <tr>
                <form id="formArch" class="form-horizontal" method="POST" action="p_email.php">
                <form  id="form_a_archivo" method="POST" action="a_archivo.php" enctype="multipart/form-data"> -->
          <form class="form-horizontal" id="formArch" method="POST" action="p_email.php" enctype="multipart/form-data">
              <input type="hidden" name="id_archivo" value="<?php echo $row[0] ?>">
              <input type="hidden" name="id_documento" value="<?php echo $row[1] ?>">
              <input type="hidden" name="nombre" value="<?php echo $row['nombre']?>">
              <input type="hidden" name="id_solicitante" value="<?php echo $row[2]?>">
              <input type="hidden" name="solicitante" value="<?php echo $row[3] ?>">
              <input type="hidden" name="email" value="<?php echo $row[4] ?>">
              <input type="hidden" name="archivo" value="<?php echo $row[5] ?>">
              <input type="hidden" name="ruta_archivo" value="<?php echo $row[6] ?>" >
              <input type="hidden" name="tipo_archivo" value="<?php echo $row[7] ?>" >
              <input type="hidden" name="date_create" value="<?php echo $row[10] ?>" >
              <td scope="row" style="text-transform: uppercase;"><?php echo $row[11]?></td>

                <!-- <td><a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>"> <?php echo $archivo?></a></td> -->
                <td align="center"><a download="<?php echo $nombre_archivo?>" href="<?php echo $ruta_archivo ?>" download="<?php echo $archivo?>" class="fa fa-download"></a></td>

                <td>
                  <select class="form-control" id="" name="status">
                    <option value="<?php echo $row[9] ?>"><?php echo $status; ?></option>
                    <option value="1">En proceso...</option>
                    <option value="2">Rechazado</option>
                    <option value="3">Validado</option>
                  </select>
                </td>

                <td>
                  <input type="text" class="form-control" name="comentario" value="<?php echo $row[8]; ?>">
                </td>
                
                <!-- <td>
                  <button type="submit" class="btn btn-outline-primary btn-block fa fa-database" id="btnSubmit"></button>
                </td> -->
                 <!--  <a class="btn btn-outline-primary btn-block" href="re_documentos.php" role="button">Guardar todo</a>
                  <button type="submit" class="btn btn-outline-primary btn-block">GUARDAR TODO</button>    -->              
              </tr>  
                <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="col-md-4 offset-md-3">
              <button type="submit" class="btn btn-outline-primary btn-block fa fa-database" id="btnSubmit"></button>
            </div>
              <!-- <div class="col-md-4 offset-md-1">
                <button type="submit" class="btn btn-outline-primary btn-block" id="btnAccept" >guardar todo</button>
              </div>
             -->
          </form>
  <?php } ?> 
</div>
  

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script type="text/javascript">

    // $(document).ready(function() {
    //    $("#formArch").bind("submit", function(e) {
    //       e.preventDefault();
    //         $.ajax({
    //           url: $(this).attr('action'),
    //           dataType: 'json',
    //           type: $(this).attr('method'),
    //           data: $(this).serialize(),
    //           beforeSend: function(){
                
    //           },
    //           success: function(respuesta) {
    //           if(respuesta.error == false) {
    //             $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
    //                setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 1500);
    //             $("#respuesta").html("enviado!");
    //             $("#mensage").html(" se actualizo correctamente, y se notifico al alumno...");
    //           }else if(respuesta.error == true){
    //             $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
    //                setTimeout(function(){ $("alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 1500);
    //             $("#respuesta").html("error! ");
    //             $("#mensage").html("el mensage NO ha sido enviado, es necesario escribir un comentario...");
    //           }
    //           },
    //           error: function(respuesta){
    //             $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
    //             $("#respuesta").html("error! ");
    //             $("#mensage").html("el mensage NO ha sido enviado");
    //           }
    //         });
    //         return false;
    //       });
    //     }); 

        $(document).on('submit', '#formArch', function(event){
          event.preventDefault();
          var formData = new FormData(this);
          $.ajax({
              dataType: 'json',
              data: formData,
              contentType: false,
              processData: false,
              cache: false,
              type: $(this).attr("method"),
              url: $(this).attr("action"),
            beforeSend: function(respuesta) {
              $('#btnSubmit').attr('disabled', 'true');
            },
            success: function(respuesta){
              if (respuesta.error == false) {
                $('#btnSubmit').removeAttr('disabled');
                  $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeIn();
                    setTimeout(function(){ $('#alerta').removeClass("hide").removeClass("alert-danger").addClass("alert-success").fadeOut(); }, 950);
                  $("#respuesta").html("enviado! ");
                  $("#mensage").html("se actualizo correctamente, y se notifico al alumno");
                }else if (respuesta.error == true){
                  $('#btnSubmit').removeAttr('disabled');
                  $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeIn();
                    setTimeout(function(){ $("alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger").fadeOut(); }, 950);
                  $("#respuesta").html("error! ");
                  $("#mensage").html("el mensage NO ha sido enviado");
                }
              
              },
              error: function(respuesta){
                $('#btnSubmit').removeAttr('disabled');
                // $("#alerta").removeClass("hide").removeClass("alert-success").addClass("alert-danger");
                // $("#respuesta").html("error!");
                // $("#mensage").html("el mensage NO ha sido enviado");
              }
            })
            return false;
          }); 

    </script>
  </body>
</html>