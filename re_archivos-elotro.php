<?php
session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
    
?>


<!DOCTYPE html>
<html lang="en">
  <head><meta charset="gb18030">

      <!---->
 
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>
  <body>
    <div class="container"><br>
       <a class="btn btn-primary offset-md-10" href="admin.php" role="button">regresar</a>
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:90px;" alt="branding logo">
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
    
    	<h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>Generar Reporte</span>
          </h6>
		     	<form method="post" class="form" action="reporte.php">
		<input type="date" name="fecha1">
		<input type="date" name="fecha2">
		<input type="submit" name="generar_reporte">
		</form>

		<br><br>
		<h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>Revisi&oacute;n de documentos</span>
          </h6>
        </div>         
      </div>
          <?php
              require("e_d.php");
              require("conexion.php");
              $curp="
              SELECT distinct * FROM archivos a
                  JOIN documentos d
                  ON a.id_documento = d.id_documento
                  JOIN usuarios u
                  ON d.id_usuario = u.id_usuario
                  group by id_solicitante ORDER BY a.date_create asc";
              $result=$mysqli->query($curp);
              if($mostrar=mysqli_fetch_array($result)){
              ?>
        <form id="userForm" class="form-horizontal" method="POST">
          <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col center" style="text-align: center;">NO.</th>
                <th scope="col center" style="text-align: center;">ID</th>
                <th scope="col center" style="text-align: center;">Nombre solicitante</th>
                <th scope="col center" style="text-align: center;">PE</th>
                <th scope="col center" style="text-align: center;">Creación</th>
                <th scope="col center" style="text-align: center;">Modificación</th>
                <th scope="col center" style="text-align: center;">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php



      $query="SELECT distinct a.id_archivo,
                  a.id_documento,
                  a.id_solicitante,
                  a.carrera,
                  a.solicitante,
                  a.email,
                  a.archivo,
                  a.ruta_archivo,
                  a.tipo_archivo,
                  a.comentario,
                  a.status,
                  a.date_create,
                  a.date_update,
                  d.nombre,
                  d.id_documento,
                  u.id_usuario FROM archivos a
                  JOIN documentos d
                  ON a.id_documento = d.id_documento
                  JOIN usuarios u
                  ON d.id_usuario = u.id_usuario
                  group by id_solicitante ORDER BY a.date_create DESC";
              $query_db=$mysqli->query($query);
              $number=1;
          while($row=mysqli_fetch_array($query_db)){
            // SELECT DISTINCT * FROM archivos WHERE id_documento in(SELECT count(id_documento) FROM documentos WHERE status != 0) and status != 0 group by status
            $consult=$mysqli->query('SELECT COUNT(id_documento) FROM documentos WHERE status != 0');
            if($rowdos=mysqli_fetch_array($consult)){
              $rowdoss = $rowdos[0] * 3;
              $rowdosss = $rowdoss + 2;
              $con=$mysqli->query("SELECT sum(status) FROM archivos WHERE id_documento in (SELECT id_documento FROM documentos WHERE status != 0) AND id_solicitante=$row[2];");
              if($rowtres=mysqli_fetch_array($con)){ 
                $rowtres[0];
                $cons=$mysqli->query("SELECT sum(status) FROM fotos where id_solicitante=$row[2];");
                if($rowcuatro=mysqli_fetch_array($cons)){ 
                $sumSin= $rowdos[0] + 1;
                $sumTot= $rowcuatro[0] + $rowtres[0];

                if ($sumTot <= $sumSin) {
                  $estatus='<p style="font-size: .9rem; color: #bd2130; text-transform: uppercase;">Documentos SIN REVISAR</p>';
                }else if ($sumTot < $rowdosss) {
                  $estatus='<p style="font-size: .9rem; color: #ffbc00; text-transform: uppercase;">Documentos en proceso...</p>';
                }else if ($sumTot >= $rowdosss) {
                  $estatus='<p style="font-size: .9rem; color: #28a745; text-transform: uppercase;">Documentos VALIDADOS</p>';
                }
              ?>
              <tr>
                <td class="center"style="text-align: center; font-weight:bold; font-size: 120%;"><?php echo $number; $number++;?></td>
                <td class="center"style="text-align: center;"><a><?php echo $row["id_solicitante"]; ?></a></td>
                
               <td class="center"style="text-align: center;"><a><?php echo utf8_decode( utf8_encode( $row["solicitante"])); ?></a></td>
                <td class="center"style="text-align: center;"><a><?php echo utf8_decode( $row["carrera"]); ?></a></td>
                <td class="center"style="text-align: center;"><a><?php echo $row["date_create"]; ?></a></td>
                <td class="center"style="text-align: center;"><a><?php echo $row["date_update"]; ?></a></td>
                <td class="center"style="text-align: center;"><a href="vm_archivos.php?id_solicitante=<?php echo SED::encryption($row["id_solicitante"])?>"><?php echo $estatus;?></a></td>
              </tr>

          <?php } } } } } ?>
            </tbody>
          </table>
        </div>
        </form><br>
        <div class="col-md-2 offset-md-5">
        <a class="btn btn-outline-primary btn-block" href="admin.php" role="button">Cancelar</a>
        </div><br><br>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
 
  </body>
</html>