<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require('conexion.php');

if (isset($_POST["id_archivo"]) && isset($_POST["id_documento"]) && isset($_POST["id_solicitante"]) && isset($_POST["solicitante"]) && isset($_POST["email"]) && isset($_POST["archivo"]) && isset($_POST["ruta_archivo"]) && isset($_POST["tipo_archivo"]) && isset($_POST["comentario"]) && isset($_POST["status"]) && isset($_POST["date_create"])) {

    $id_archivo=$mysqli->real_escape_string($_POST["id_archivo"]);
    $id_documento=$mysqli->real_escape_string($_POST["id_documento"]);
    $id_solicitante=$mysqli->real_escape_string($_POST["id_solicitante"]);
    $solicitante=$mysqli->real_escape_string($_POST["solicitante"]);
    $email=$mysqli->real_escape_string($_POST["email"]);
    $archivo=$mysqli->real_escape_string($_POST["archivo"]);
    $ruta_archivo=$mysqli->real_escape_string($_POST["ruta_archivo"]);
    $tipo_archivo=$mysqli->real_escape_string($_POST["tipo_archivo"]);
    $comentario=$mysqli->real_escape_string($_POST["comentario"]);
    $status=$mysqli->real_escape_string($_POST["status"]);
    $statuss=$mysqli->real_escape_string($_POST["status"]);
    $nombre=$mysqli->real_escape_string($_POST["nombre"]);
    $date_create=$mysqli->real_escape_string($_POST["date_create"]);
    date_default_timezone_set('America/Mexico_city');
    $date_update=date("y-m-d H:i:s");


  if ($statuss == 1) {
        $statuss='se encuentra en proceso, espere noticias';
  }elseif ($statuss == 2) {
        $statuss='fue validado, Gracias por cumplir con los requisitos solicitados';
  }elseif ($statuss == 3){
       $statuss='fue rechazado por no cumplir con los requisitos solicitados';
  }

  if (strlen($id_archivo)==0 || !isset($_POST["id_archivo"])) {
      echo json_encode(array('error' => true));
    }if (strlen($id_documento)==0 || !isset($_POST["id_documento"])) {
      echo json_encode(array('error' => true));
    }if (strlen($comentario)==0 || !isset($_POST["comentario"])) {
      echo json_encode(array('error' => true));
    } else {  
    $res=$mysqli->query("UPDATE archivos SET id_archivo = '$id_archivo', id_documento ='$id_documento', id_solicitante ='$id_solicitante', solicitante ='$solicitante', email ='$email', archivo ='$archivo', ruta_archivo ='$ruta_archivo', tipo_archivo ='$tipo_archivo', comentario ='$comentario', status ='$status', date_create='$date_create', date_update='$date_update' where id_archivo = '$id_archivo'");
     

    $mail = new PHPMailer(true);

    try {
        //Administrador
        $administradorEmail = 'javiercasimiro@utsem-morelos.edu.mx';
        $cuerpo = '
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      </head>
      <body style="font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji">
          <h5 style="margin-left: 10%; margin-top: 4%; font-size: 1.25rem;">100% BUHOS...</h5><br>
          <div align="center">
            <img src="https://i.postimg.cc/VvVJwQjY/logo-utsem-png.png" style="width:100px;" alt="branding logo">
          </div> 
          <div align="center" style="margin-top: 2%; margin-bottom: 5%;">
            <h3>Hola! <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$solicitante.'</small></h3>
          </div>
            <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">Hemos reviso tu documentación y el documento llamado: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller; text-transform: uppercase;">'.$nombre.'</small></h5>
            <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">El cual: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$statuss.'</small></h5>
            <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">Comentario: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$comentario.'</small></h5>
            <h4 style="margin-left: 5%; margin-bottom: .2rem; font-weight: 500; line-height: 1;">Fecha de verificación: <small style="color: #6c757d!important; font-size: 90%; font-size: smaller;">'.$date_create.'</small></h5>
          <div align="center">
            <a href="http://admisionut.utsem-morelos.edu.mx/" style="background-color: #00a48d; border-color: #00a48d; color: #FFF; margin-top: 10%; display: inline-block; font-weight: 400; text-align: center; vertical-align: middle; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; border: 1px solid transparent; padding: .375rem .75rem; font-size: 1rem; line-height: 1.5; border-radius: .25rem; transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;">Conocenos</a> 
          </div>   
      </body>
    </html>';

        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'smtp.gmail.com;smtp.live.com;mail.yahoo.com;plus.smtp.mail.yahoo.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'chirinos.jesus1234@gmail.com';                     // SMTP username
        $mail->Password   = 'chirinoscuevas1998';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients      
        $mail->setFrom('chirinos.jesus1234@gmail.com', 'Universidad Tecnológica del Sur del Estado de Morelos UTSEM');
        //$mail->addAddress('chirinos.jesus1234@gmail.com', $solicitante);
        $mail->addAddress($administradorEmail, $solicitante);     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        // Content
        $mail->Timeout = 5;
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Revision documentos de admision UTSEM';
        //$mail->msgHTML(file_get_contents($ruta_archivo),dirname(__FILE__));
        $mail->Body    = $cuerpo;

      //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      //$mail->send();
      //echo "Mailer Error: " . $mail->ErrorInfo;
      //echo json_encode(array('error' => 1));

      $exito = $mail->Send();
      $intentos=1; 
      while ((!$exito) && ($intentos < 2)) {
      sleep(5);
      $exito = $mail->Send();
      $intentos=$intentos+1;  
      }if(!$exito){
      echo $mail->ErrorInfo;
      echo json_encode(array('error' => true));  
      }else{ 
      echo json_encode(array('error' => false));
      }

      } catch (Exception $e) {
      //echo json_encode(array('error' => 0));
      }
       echo json_encode(array('error' => false));
    }
  }else{ 
    echo json_encode(array('error' => true));
  }
$mysqli->close();
?>