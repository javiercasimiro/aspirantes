<?php

session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }





?>


<!DOCTYPE html>
<html lang="en">
  <head><meta charset="gb18030">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
	<title>UTSEM</title>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="librerias_buscar/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="librerias_buscar/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="librerias_buscar/css/buttons.dataTables.min.css">
</head>
<body>
    
    <header>
        <!-- <h1 class="text-center text-light">Tutorial</h1>-->
         <!--<h2 class="text-center text-light">Cómo usar <span class="badge badge-danger">DATATABLES</span></h2> -->
     </header> 
    <div style="height:50px"></div>
    
    <!--Ejemplo tabla con DataTables-->
    <div class="container">
        <a class="btn btn-primary offset-md-10" href="admin.php" role="button">regresar</a>
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:90px;" alt="branding logo">
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          
        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>Generar Reporte</span>
          </h6>
		<form method="post" class="form" action="reporte.php">
		    <div class="form-row justify-content-md-center">
                <div class="form-group col-md-3">
                    <input class="form-control" type="date" name="fecha1">
                </div>
                <div class="form-group col-md-3">
                  <input class="form-control" type="date" name="fecha2">
                </div>
            </div>
            <input type="submit" class="btn btn-outline-success" name="generar_reporte">
		</form>
		<br>
<br>
  <a href="comprimir/comprimir.php">Descargar Fotos</a>

    </div> 
    </div>
    
  
<br>
<br>

 <div class="col-md-2 offset-md-5">
        <a class="btn btn-outline-primary btn-block" href="admin.php" role="button">Cancelar</a>
        </div><br><br>
      </div>

</body>
</html>