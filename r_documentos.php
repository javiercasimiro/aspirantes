<?php
session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>

    <!-- BEGIN: Content-->
    <div class="container" style="padding-top: 10%;">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-header" style="text-align: center;">
                        <h4 class="card-title" id="basic-layout-form">Registro de Documento</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form name="a_documento" method="POST" action="a_documentos.php">
                                <div class="form-body offset-md-3">
                                    <div class="row">                                                
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Nombre del documento" style="text-transform: capitalize;" name="nombre" required="">
                                            </div>
                                        </div>                                                  
                                    </div>



                                    <div class="row">                                                
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Descripción" style="text-transform: capitalize;" name="descripcion"></textarea>
                                            </div>
                                        </div>                                                  
                                    </div>

                                      <label>¿Documento obligatorio?</label><br>
                                      <input type="radio" class="op" name="obligatorio" value="si" checked> Si<br>
                                      <input type="radio" class="op" name="obligatorio" value="no"> No<br>

                                      <br>

                                    <input type="hidden" name="id_usuario" value="<?php echo $_SESSION['id_usuario']?>" >
                                    <div class="form-actions offset-md-3">
                                        <a class="btn btn-warning" href="re_documentos.php" role="button">Salir</a>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-check-square-o"></i> Guardar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
</body>
<!-- END: Body-->

</html>