<?php
require_once('./lib/nusoap.php');
session_start();
/*$lsMatricula = '20209';
$lsCiclo     = 'SEP-DIC 20';*/
$lsMatricula = (isset($_POST['matricula']) ? $_POST['matricula']: '');
$lsCiclo     = (isset($_POST['ciclo']) ? $_POST['ciclo'] : '');
$lsCurp		 = (isset($_POST['CURP']) ? $_POST['CURP']: '');
$strMsg = '';
if (strlen($lsMatricula)==0) {
	$strMsg = 'Debes proporcionar tu matricula<br>';
}
if (strlen($lsCurp)==0) {
	$strMsg = 'Debes proporcionar tu CURD<br>';
}
if (strlen($lsCiclo)==0 || $lsCiclo == '#') {
	$strMsg = 'Debes indicar el periodo<br>';
}
if (strlen($strMsg)==0) {
	$client = new nusoap_client("http://www.mi-escuelamx.com/ws/wsUTSEM/Datos.asmx?wsdl", 'wsdl', '', '', '', '');
	$err = $client->getError();
	if ($err) {
		echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	}
	$params = array(
		'lsMatricula' => $lsMatricula,
		'lsCurp' => $lsCurp,
		'lsCiclo'     => $lsCiclo
	);
	$result = $client->call('Admisiones', $params);
	if ($result) {
		$data = $result['AdmisionesResult']['diffgram'];
		if ($data == '' || count($data) == 0) {
			$return['error'] = array('error' => 1, 'mensaje' => 'matricula O CURP NO existen');
		}else{
			$_SESSION['solicitante'] = $data['NewDataSet']['Admisiones'];
			$return['error'] = array('error' => 0, 'mensaje' => 'Usuario autenticado');
		}
	}
}else{
	$return['error'] = array('error' => 1, 'mensaje' => $strMsg);
}

echo json_encode($return);
?>
