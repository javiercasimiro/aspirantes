<?php 
  session_start();
  if($_SESSION['solicitante'] != ''){
    header('Location: user-form.php');
  }
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>
  <body>
  <div class="col-12 d-flex align-items-center justify-content-center">
    <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0"><br><br>
      <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
        <div class="card-header border-0" style="background-color: white;">
          <div class="card-title text-center">
            <img src="img/logo-utsem.png" style="width:150px;" alt="branding logo">
          </div>         
          <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
            <span><h6 style="text-transform: uppercase;"><b>Bienvenido</b>, ingrese sus datos.</h6>"Usuario y Contraseña"<br> Si tienes algún problema con la plataforma, escribenos a soporte@utsem-morelos.edu.mx</span>
          </h6>
          <div id="loginInfo" style="display: none;">
            <div class="alert alert-danger" role="alert" id="alert">
              
            </div>
          </div>
        </div>
        <div class="card-content" id="userFormContainer">
          <div class="card-body" style="padding-top: 0;">
            <form id="userForm" class="form-horizontal"  method="POST" action="soaplogin.php">
              <fieldset class="form-group position-relative has-icon-left">
                <input type="text" class="form-control" id="matricula" name="matricula"
                  placeholder="Ingrese su matricula" required>
                <div class="form-control-position">
                </div>
              </fieldset>
              <fieldset class="form-group position-relative has-icon-left">
                <input type="text" class="form-control" id="CURP" name="CURP"
                  placeholder="Ingrese CURP" required>
                <div class="form-control-position">
                </div>
              </fieldset>
              <fieldset class="form-group position-relative">
                <div class="form-control-position">
                  <select class="form-control" id="ciclo" name="ciclo" >
                    <option value="#">Seleccione Periodo</option>
                    <option value="SEP-DIC 20">SEP-DIC 20</option>
                  </select>
                </div>
              </fieldset>
              <input type="submit" class="btn btn-outline-primary btn-block" id="btnSubmit" value="Ingresar">
               <!-- <a class="btn btn-outline-primary btn-block" href="user-form.php" role="button">Ingresar</a> -->
               <a class="btn btn-outline-primary btn-block" href="index.php" role="button">Regresar</a>
             <!-- <button type="submit" class="btn btn-outline-primary btn-block" id="btnAccept">Buscar</button>-->
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script>
      $(function(){
        $('#userForm').submit(function(e){
          e.preventDefault();
          var postData = $(this).serialize();
          $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            dataType: 'json',
            data: postData,
            beforeSend: function(){
              $('#btnSubmit').val('Validando usuario...');
              $('#btnSubmit').attr('disabled', 'true');
            },
            success: function(respuesta) {
              if (respuesta.error.error == 1) {
                $('#btnSubmit').val('Reiniciar');
                $('#btnSubmit').removeAttr('disabled');
                $('#alert').html(respuesta.error.mensaje);
                $('#loginInfo').fadeIn();
                setTimeout(function(){ $('#loginInfo').fadeOut(); }, 2500);
              }else if(respuesta.error.error == 0){
                document.location.href = 'user-form.php';
              }
            },
            error: function(jqXHR, textStatus, errorThrow){
              $('#btnSubmit').val('Reiniciar');
              $('#btnSubmit').removeAttr('disabled');
              console.log(jqXHR);
            }
          })
        })
      });
    </script>
  </body>
</html>