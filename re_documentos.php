<?php
require("e_d.php");
require("conexion.php");
session_start();
if (@!$_SESSION['id_usuario'] && (!isset($_SESSION['id_usuario']) && empty($_SESSION['id_usuario'])) || $_SESSION['id_usuario'] === 1) {
        header("Location: l-admin.php");
        exit;
    }
    if ($_SESSION['status'] == '0') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }else if ($_SESSION['status'] >= '1') {
      $nombre_usu=$_SESSION['nombre_usu'];
      $id_usuario=$_SESSION['id_usuario'];
      $status = $_SESSION['status'];
      $correo_usu = $_SESSION['correo_usu'];
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo-utsem.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title>UTSEM</title>
  </head>
  <body>
    <div class="container"><br>
      <a class="btn btn-primary offset-md-10" href="admin.php" role="button">regresar</a>
      <div class="card-header border-0" style="background-color: white;">
        <div class="card-title text-center">
          <img src="img/logo-utsem.png" style="width:90px;" alt="branding logo">
          <h3 style="text-align: center;">Sistema de Admisión Online</h3>
          <h6 class="card-subtitle line-on-side text-muted text-center font-small-3">
            <span>Control de documentos</span>
          </h6><br><br>
        </div>         
      </div>
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content">
              <div class="content-body">
                <section class="row all-contacts">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-head">
                        <div class="card-header">
                          <a class="btn btn-primary offset-md-10" href="r_documentos.php" role="button">Nuevo Documento</a>
                        </div>
                      </div>
                      <div class="card-content">
                        <div class="card-body">
                        <!-- Task List table -->
                          <div class="table-responsive">
                            <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle text-center">
                            <thead style="background-color:white;">
                              <tr>
                                <th>NO.</th>
                                <th>Nombre</th>
                                <th>¿Obligatorio?</th>
                                <th>Descripción</th>
                                <th>Creación</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                                <th>Archivar</th>
                              </tr>
                            </thead>
                            <tbody>
                               
                              <?php
                              $sql="SELECT * FROM documentos WHERE status>=1 ORDER BY nombre ASC";
                              $result=$mysqli->query($sql);
                              $number=1;
                              while($mostrar=mysqli_fetch_array($result)){
                                   
                              ?>

                              <tr>
                                <td>
                                  <div class="media-body media-middle">
                                   <a class="media-heading name"><?php echo $number; $number++;?></a>
                                   </div>
                                </td>
                                  
                                  
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['nombre']?></a>
                                  </div>
                                </td>



                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['obligatorio']?></a>
                                  </div>
                                </td>
                                


                                
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['descripcion']?></a>
                                  </div>
                                </td>
                                <td>


                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['date_create']?></a>
                                  </div>
                                </td>
                                <td>
                                  <form action="vm_documentos.php">
                                      <button type="submit" name="id_documento" value="<?php echo SED::encryption($mostrar['id_documento'])?>" class="fa fa-wrench btn btn-warning">
                                  </form>
                                </td><td>
                                  <form name="eliminar-documento" method="POST" action="e_documentos.php">
                                    <button type="submit" name="id_documento"  value="<?php echo $mostrar['id_documento']?>" class="fa fa-times btn btn-danger">
                                  </form>
                                </td><td>
                                  <form name="eliminar-documento" method="POST" action="ma_documentos.php">
                                    <button type="submit" name="id_documento"  value="<?php echo $mostrar['id_documento']?>" class="fa fa-folder btn btn-light">
                                  </form>
                                </td>                                                       
                              </tr>
                                 <?php
                                }
                                ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section><br><br>

              <section class="row all-contacts">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-head">
                        <div class="card-header">
                          DOCUMENTOS ARCHIVADOS
                        </div>
                      </div>
                      <div class="card-content">
                        <div class="card-body">
                        <!-- Task List table -->
                          <div class="table-responsive">
                            <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle text-center">
                            <thead style="background-color:white;">
                              <tr>
                                <th>NO.</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Creación</th>
                                <th>Modificación</th>
                                <th>Visualizar</th>
                                <!-- <th>eliminar</th> -->
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              $sql="SELECT * FROM documentos WHERE status=0";
                              // SELECT * FROM documentos d join usuarios u
                              // ON d.id_usuario=u.id_usuario WHERE d.id_usuario='$id_usuario' and d.status=0
                              $result=$mysqli->query($sql);
                              $numberDos=1;
                              while($mostrar=mysqli_fetch_array($result)){
                                
                              ?>

                              <tr>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $numberDos; $numberDos++; ?></a>
                                  </div>
                                </td>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['nombre']?></a>
                                  </div>
                                </td>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['descripcion']?></a>
                                  </div>
                                </td>
                                 <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['date_create']?></a>
                                  </div>
                                </td>
                                <td>
                                  <div class="media-body media-middle">
                                  <a class="media-heading name"><?php echo $mostrar['date_update']?></a>
                                  </div>
                                </td>
                                <!-- <td>
                                  <form action="vm_documentos.php">
                                      <button type="submit" name="id_documento" value="<?php echo SED::encryption($mostrar['id_documento'])?>" class="fa fa-wrench btn btn-warning">
                                  </form>
                                </td> -->
                                <td>
                                  <form name="eliminar-documento" method="POST" action="md_documentos.php" >
                                    <button type="submit" name="id_documento" value="<?php echo $mostrar['id_documento']?>" class="fa fa-reply-all btn btn-dark">
                                    <!-- <button type="submit" name="id_documento" onclick="liberar()" value="<?php echo $mostrar['id_documento']?>" class="fa fa-reply-all btn btn-dark"> -->
                                  </form>
                                </td>                                                       
                              </tr>
                                 <?php
                                }
                                ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
    </div><br><br>
    <!-- END: Content-->
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript">
      // function liberar() {
      //   <?php
      //     $id_documento=$_POST['id_documento'];
          
      //      $actualiza=$mysqli->query("UPDATE documentos SET status=1 WHERE id_documento='$id_documento'");
      //      header('location:re_documentos.php');
      //     ?>
      // }
    </script>
  </body>
</html>